﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll;
using S_eMotion_TEAMS.Messenges;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelMessagesAbstraction;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelReceivedMesseges;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelSendMessages;
using S_eMotion_TEAMS.Model.Entities;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace S_eMotion_TEAMS.ControlPanel
{
    public class CommunicationManager
    {
        public ConnectionInfo ConnectionInfo { get; set; }

        public SerialPort SerialPort { get; set; }

        public CommunicationManager(ConnectionInfo connectionInfo)
        {
            try
            {
                ConnectionInfo = connectionInfo;

                Messenger.Default.Register<PlayerSendMessage>(this, (action) => Send(action));
                Messenger.Default.Register<PeriodeAndGameDirectionSendMessage>(this, (action) => Send(action));
                Messenger.Default.Register<SendTeamsMessage>(this, (action) => SendTeams(action));


                if (string.IsNullOrWhiteSpace(ConnectionInfo.PortName))
                {
                    ConnectionInfo.ConnectionState = ConnectionState.None;
                }
                else
                {
                    SerialPort = new SerialPort(ConnectionInfo.PortName, 9600, Parity.None, 8, StopBits.One);

                    SerialPort.DataReceived += new SerialDataReceivedEventHandler(Received);
                    SerialPort.ReadTimeout = 500;
                    SerialPort.WriteTimeout = 500;
                    SerialPort.Open();

                    ConnectionInfo.InitializeTimer();
                }
            }
            catch (Exception ex)
            {
                Messenger.Default.Send(new ErrorFeedMessage(string.Format("Fehler beim aufbauen der Verbindung zum Bedinpult, bitte überprüfen Sie den ausgwählten Com-Port. Error: {0}, InnerExeption: {1}", ex.Message, ex.InnerException)));
            }
        }

        private void SendTeams(SendTeamsMessage action)
        {
            foreach (Team team in action.TeamsToSend)
            {
                foreach (Player player in team.TeamMember)
                {
                    Send(new PlayerSendMessage(player));
                    Thread.Sleep(500);
                }
            }
        }

        private void Send(IControlPanelMessage controlPanelMessage)
        {
            try
            {
                STX_ETX_Protocol protocol = new STX_ETX_Protocol(controlPanelMessage.GetPayLoad(), controlPanelMessage.GetAdressByte());

                if (SerialPort == null)
                    return;
                //if (!SerialPort.IsOpen)
                //    SerialPort.Open();
                //SerialPort.Write(protocol.GetProtocol().ToString());
            }
            catch (Exception ex)
            {
                Messenger.Default.Send(new ErrorFeedMessage(string.Format("Fehler während des Sendens zum Bedienpult. Error: {0}, InnerExeption: {1}", ex.Message, ex.InnerException)));
            }
        }

        private void Received(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                string data = SerialPort.ReadLine();
                List<byte> bytes = new List<byte>();
                foreach (char c in data)
                    bytes.Add((byte)c);

                STX_ETX_Protocol protocol = new STX_ETX_Protocol(bytes.ToArray());

                switch (protocol.GetAdressByte())
                {
                    case AdressByte.HeimPlayer:
                    case AdressByte.GastPlayer:
                        Messenger.Default.Send(new PlayerSendMessage(protocol.GetPayLoad(), protocol.GetAdressByte()));
                        break;
                    case AdressByte.SportKind:
                        Messenger.Default.Send(new SportKindReceivedMessage(protocol.GetPayLoad()));
                        break;
                    case AdressByte.GameTimeAndHorn:
                        Messenger.Default.Send(new GameTimeAndHornReceivedMessage(protocol.GetPayLoad()));
                        break;
                    case AdressByte.PeriodeAndGameDirection:
                        Messenger.Default.Send(new PeriodeAndGameDirectionReceivedMessage(protocol.GetPayLoad()));
                        break;
                    case AdressByte.Score:
                        Messenger.Default.Send(new ScoreReceivedMessage(protocol.GetPayLoad()));
                        break;
                    case AdressByte.TeamFouls:
                        Messenger.Default.Send(new TeamFoulsReceivedMessage(protocol.GetPayLoad()));
                        break;
                    case AdressByte.TeamNames:
                        Messenger.Default.Send(new TeamNamesReceivedMessage(protocol.GetPayLoad()));
                        break;
                    case AdressByte.None:
                    default:
                        throw new Exception("Adressbyte was not set or Message got not implemented yet.");
                }

                ConnectionInfo.TimerReset();
            }
            catch (Exception ex)
            {
                Messenger.Default.Send(new ErrorFeedMessage(string.Format("Fehler beim Empfangen von Bedinpultdaten. Error: {0}, InnerExeption: {1}", ex.Message, ex.InnerException)));
            }
        }
    }
}
