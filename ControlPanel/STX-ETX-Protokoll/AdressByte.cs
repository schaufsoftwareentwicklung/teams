﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll
{
    public enum AdressByte
    {
        None = 0x00,
        SportKind = 0x20,
        GameTimeAndHorn = 0x21,
        PeriodeAndGameDirection = 0x22,
        Score = 0x23,
        TeamFouls = 0x27,
        TeamNames = 0x29,
        HeimPlayer = 0x60,
        GastPlayer = 0x61
    }
}
