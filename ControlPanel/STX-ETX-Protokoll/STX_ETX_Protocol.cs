﻿using S_eMotion_TEAMS.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll
{
    public class STX_ETX_Protocol
    {
        //Startbyte of protocol
        private byte STX { get => 0x02; }
        //determines the protocoltype
        private AdressByte AdressByte { get; set; }
        //Key to the EncryptionRotion
        private byte EncryptionKey { get; set; }
        private byte[] Payload { get; set; }
        //Endbyte of protocol
        private byte ETX { get => 0x03; }

        public STX_ETX_Protocol(byte[] receivedData)
        {
            if (receivedData[0] != STX || receivedData[receivedData.Length - 1] != ETX)
                throw new Exception("Empfangenes Protocoll ist nicht vollständig");

            AdressByte = (AdressByte)receivedData[1];
            EncryptionKey = receivedData[2];

            byte[] bytesToEncrypt = new byte[receivedData.Length - 4];
            Array.Copy(receivedData, 3, bytesToEncrypt, 0, receivedData.Length - 4);

            Payload = STX_ETX_Encryption.Decrypt(EncryptionKey, bytesToEncrypt);
        }

        public STX_ETX_Protocol(byte[] payload, AdressByte adressByte)
        {
            AdressByte = adressByte;
            Payload = payload;
        }

        public byte[] GetProtocol()
        {
            Random random = new Random();
            EncryptionKey = (byte)random.Next(48, 57);
            STX_ETX_Encryption.Encrypt(EncryptionKey, Payload);

            var bytes = new List<byte>();
            bytes.Add(STX);
            bytes.Add((byte)AdressByte);
            bytes.Add(EncryptionKey);
            foreach(byte b in Payload)
                bytes.Add(b);
            bytes.Add(ETX);

            return bytes.ToArray();
        }

        public AdressByte GetAdressByte()
        {
            return AdressByte;
        }

        public byte[] GetPayLoad()
        {
            return Payload;
        }
    }
}
