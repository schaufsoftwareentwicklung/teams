﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll
{
    public static class STX_ETX_Encryption
    {
        private static readonly Dictionary<byte, byte[]> EncryptionTable = new Dictionary<byte, byte[]>()
        {
            { 0x30 , new byte[4] { 0x85, 0xBA, 0x90, 0xA5 } },
            { 0x31 , new byte[4] { 0xB1, 0xA2, 0x9F, 0x8B } },
            { 0x32 , new byte[4] { 0xAA, 0xB2, 0xA7, 0x99 } },
            { 0x33 , new byte[4] { 0x97, 0xA6, 0xB3, 0x85 } },
            { 0x34 , new byte[4] { 0x9D, 0x84, 0xBC, 0xAD } },
            { 0x35 , new byte[4] { 0x8F, 0xA8, 0xBD, 0xB1 } },
            { 0x36 , new byte[4] { 0x9C, 0xA0, 0x87, 0xBA } },
            { 0x37 , new byte[4] { 0xC1, 0xC7, 0x99, 0x88 } },
            { 0x38 , new byte[4] { 0xB2, 0x86, 0x95, 0x84 } },
            { 0x39 , new byte[4] { 0x83, 0xB7, 0x81, 0xB0 } }
        };

        public static byte[] Encrypt(byte encryptKey ,byte[] dataToEncrypt)
        {
            var keysToEncrypt = EncryptionTable[encryptKey];

            for (int i = 0; i < dataToEncrypt.Length; i++)
            {
                dataToEncrypt[i] = (byte)(dataToEncrypt[i] ^ keysToEncrypt[i % 4]);
            }

            return dataToEncrypt;
        }

        public static byte[] Decrypt(byte decryptKey, byte[] dataToDecrypt)
        {
            var keysToEncrypt = EncryptionTable[decryptKey];

            for (int i = 0; i < dataToDecrypt.Length; i ++)
            {
                dataToDecrypt[i] = (byte)(dataToDecrypt[i] ^ keysToEncrypt[i % 4]);
            }

            return dataToDecrypt;
        }
    }
}
