﻿using Caliburn.Micro;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using S_eMotion_TEAMS.Messenges;
using S_eMotion_TEAMS.Model.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.ViewModels
{
    public class OptionsViewModel : ViewModelBase
    {
        public RelayCommand ReloadCOMPortsButton { get; private set; }

        private bool _sendCompleteTeams;
        private string _selectedCOMPort;
        private List<string> _COMPorts;
        private Sport _selectedSport;
        private int _durationLatestFouler;

        public bool SendCompleteTeams
        {
            get => _sendCompleteTeams;
            set
            {
                _sendCompleteTeams = value;
                RaisePropertyChanged(nameof(SendCompleteTeams));
                Properties.Settings.Default.SendCompleteTeams = _sendCompleteTeams;
            }
        }

        public string SelectedCOMPort
        {
            get => _selectedCOMPort;
            set
            {
                _selectedCOMPort = value;
                RaisePropertyChanged(nameof(SelectedCOMPort));
                Properties.Settings.Default.SelectedComPort = _selectedCOMPort;
            }
        }

        public List<string> COMPorts
        {
            get => _COMPorts;
            set
            {
                _COMPorts = value;
                RaisePropertyChanged(nameof(COMPorts));
            }
        }

        public Sport SelectedSport
        {
            get => _selectedSport;
            set
            {
                _selectedSport = value;
                RaisePropertyChanged(nameof(SelectedSport));
            }
        }

        public int DurationLatestFouler
        {
            get => _durationLatestFouler;
            set
            {
                if (value < 1)
                    value = 1;
                _durationLatestFouler = value;
                Properties.Settings.Default.DurationLatestFouler = _durationLatestFouler;
                RaisePropertyChanged(nameof(DurationLatestFouler));
            }
        }


        public OptionsViewModel(Sport selectedSport)
        {
            ReloadCOMPortsButton = new RelayCommand(() => ExecuteReloadCOMPortsButton());

            ExecuteReloadCOMPortsButton();

            SelectedSport = selectedSport;

            if (COMPorts.Contains(Properties.Settings.Default.SelectedComPort))
                SelectedCOMPort = Properties.Settings.Default.SelectedComPort;
            else
                SelectedCOMPort = string.Empty;

            SendCompleteTeams = Properties.Settings.Default.SendCompleteTeams;
            DurationLatestFouler = Properties.Settings.Default.DurationLatestFouler;

            Messenger.Default.Register<SportChangedMessage>(this, (action) => SelectedSport = action.Sport);
        }

        private void ExecuteReloadCOMPortsButton()
        {
            COMPorts = SerialPort.GetPortNames().ToList();
        }
    }
}