﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using S_eMotion_TEAMS.Messenges;
using S_eMotion_TEAMS.Model.Entities;
using System.Collections.ObjectModel;
using System.Linq;

namespace S_eMotion_TEAMS.ViewModels
{
    public class EditTeamViewModel : ViewModelBase
    {
        public RelayCommand MovePlayerUpCommand { get; private set; }
        public RelayCommand MovePlayerDownCommand { get; private set; }
        public RelayCommand SortPlayersByNameCommand { get; private set; }
        public RelayCommand SortPlayersByNumberCommand { get; private set; }
        public RelayCommand AddPlayerCommand { get; private set; }


        private Player _selectedPlayer;
        private ObservableCollection<Player> _players;
        private Sport _selectedSport;

        public ObservableCollection<Player> Players
        {
            get => _players;
            set
            {
                _players = value;
                RaisePropertyChanged(nameof(Players));
            }
        }
        public Player SelectedPlayer
        {
            get => _selectedPlayer;
            set
            {
                _selectedPlayer = value;
                this.MovePlayerUpCommand.RaiseCanExecuteChanged();
                this.MovePlayerDownCommand.RaiseCanExecuteChanged();
                RaisePropertyChanged(nameof(SelectedPlayer));
                Messenger.Default.Send(new CanMovePlayerToSquadMessage());
            }
        }

        public Sport SelectedSport
        {
            get => _selectedSport;
            set { 
                _selectedSport = value;
                RaisePropertyChanged(nameof(SelectedSport));
            }
        }


        public EditTeamViewModel()
        {
            MovePlayerUpCommand = new RelayCommand(MovePlayerUp, CanMoveUp);
            MovePlayerDownCommand = new RelayCommand(MovePlayerDown, CanMoveDown);
            SortPlayersByNameCommand = new RelayCommand(SortPlayersByName);
            SortPlayersByNumberCommand = new RelayCommand(SortPlayersByNumber);


            Messenger.Default.Register<SportChangedMessage>(this, (action) => SelectedSport = action.Sport);
        }

        public bool CanMoveUp()
        {
            if (SelectedPlayer == null)
                return false;
            else
                return Players.IndexOf(SelectedPlayer) > 0;
        }

        public bool CanMoveDown()
        {
            if (SelectedPlayer == null)
                return false;
            else
                return Players.IndexOf(SelectedPlayer) + 1 < Players.Count;
        }

        public void MovePlayerUp()
        {
            var player = SelectedPlayer;
            var i = Players.IndexOf(SelectedPlayer);
            Players.Remove(SelectedPlayer);
            Players.Insert(i - 1, player);
            SelectedPlayer = player;
        }

        public void MovePlayerDown()
        {
            var player = SelectedPlayer;
            var i = Players.IndexOf(SelectedPlayer);
            Players.Remove(SelectedPlayer);
            Players.Insert(i + 1, player);
            SelectedPlayer = player;
        }


        public void SortPlayersByName()
        {
            Players.Sort(x => x.Name);
        }

        public void SortPlayersByNumber()
        {
            Players.Sort(x => x.Number);
        }
    }
}
