﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using S_eMotion_TEAMS.ControlPanel;
using S_eMotion_TEAMS.Messenges;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelReceivedMesseges;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelSendMessages;
using S_eMotion_TEAMS.Model.Entities;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace S_eMotion_TEAMS.ViewModels
{
    class GameViewModel : ViewModelBase
    {
        readonly static VirtualDisplayViewModel _virtualDisplayViewModel = new VirtualDisplayViewModel();
        //readonly static PlayersForGameViewModel _heimPlayersForGameViewModel = new PlayersForGameViewModel();
        //readonly static PlayersForGameViewModel _gastPlayersForGameViewModel = new PlayersForGameViewModel();


        public RelayCommand SendHeimTeamCommand { get; private set; }
        public RelayCommand SetPlayDirectionToHeimCommand { get; private set; }
        public RelayCommand SetPlayDirectionToGastCommand { get; private set; }
        public RelayCommand SendGastTeamCommand { get; private set; }
        public CommunicationManager CommunicationManager { get; set; }

        private Sport _selectedSport;
        private ObservableCollection<Team> _heimTeams;
        private ObservableCollection<Team> _gastTeams;
        private Team _selectedHeimTeam;
        private Team _selectedGastTeam;
        private ConnectionInfo _connectionInfo;
        private bool _gameIsRunning;
        private PlayDirection _playDirection;
        private string _errorFeed;

        public ObservableCollection<Team> HeimTeams
        {
            get => _heimTeams;
            set
            {
                _heimTeams = value;
                RaisePropertyChanged(nameof(HeimTeams));
            }
        }
        public ObservableCollection<Team> GastTeams
        {
            get => _gastTeams;
            set
            {
                _gastTeams = value;
                RaisePropertyChanged(nameof(GastTeams));
            }
        }

        public Team SelectedHeimTeam
        {
            get => _selectedHeimTeam;
            set
            {
                if (_selectedHeimTeam != null)
                    GastTeams?.Add(_selectedHeimTeam);
                if (value != null)
                    GastTeams.Remove(GastTeams.Where(x => x.TeamName == value.TeamName).FirstOrDefault());
                RaisePropertyChanged(nameof(GastTeams));

                SetTeamTyp(_selectedHeimTeam, TeamTyp.NoInfo);
                _selectedHeimTeam = value;
                SetTeamTyp(_selectedHeimTeam, TeamTyp.Heim);

                RaisePropertyChanged(nameof(SelectedHeimTeam));

                if (HeimPlayersForGameViewModel != null)
                {
                    if (value == null)
                        HeimPlayersForGameViewModel.Players = new ObservableCollection<Player>();
                    else
                        HeimPlayersForGameViewModel.Players = SelectedHeimTeam.TeamMember;
                }

                _selectedHeimTeam?.SetPlayerPositionIndexes();
                SelectedHeimTeam?.IsAktivePlayerChanged(SelectedSport.MaxAktivePlayer);
                SendHeimTeamCommand?.RaiseCanExecuteChanged();
            }
        }
        public Team SelectedGastTeam
        {
            get => _selectedGastTeam;
            set
            {
                if (_selectedGastTeam != null)
                    HeimTeams?.Add(_selectedGastTeam);
                if (value != null)
                    HeimTeams.Remove(HeimTeams.Where(x => x.TeamName == value.TeamName).FirstOrDefault());
                RaisePropertyChanged(nameof(HeimTeams));

                SetTeamTyp(_selectedGastTeam, TeamTyp.NoInfo);
                _selectedGastTeam = value;
                SetTeamTyp(_selectedGastTeam, TeamTyp.Gast);

                RaisePropertyChanged(nameof(SelectedGastTeam));

                if (GastPlayersForGameViewModel != null)
                {
                    if (value == null)
                        GastPlayersForGameViewModel.Players = new ObservableCollection<Player>();
                    else
                        GastPlayersForGameViewModel.Players = SelectedGastTeam.TeamMember;
                }

                _selectedGastTeam?.SetPlayerPositionIndexes();
                SelectedGastTeam?.IsAktivePlayerChanged(SelectedSport.MaxAktivePlayer);
                SendGastTeamCommand?.RaiseCanExecuteChanged();
            }
        }
        public ConnectionInfo ConnectionInfo
        {
            get => _connectionInfo;
            set
            {
                _connectionInfo = value;
                RaisePropertyChanged(nameof(ConnectionInfo));
            }
        }
        public Sport SelectedSport
        {
            get => _selectedSport;
            set
            {
                _selectedSport = value;
                RaisePropertyChanged(nameof(SelectedSport));
                LoadDefaultTeams();
                CheckAktivePlayerAmountOfTeam();
                if (_selectedSport.KindOfSport != KindOfSport.Basketball)
                    PlayDirection = PlayDirection.None;
                else
                    PlayDirection = PlayDirection.Left;
            }
        }
        public PlayDirection PlayDirection
        {
            get => _playDirection;
            set
            {
                _playDirection = value;
                SetPlayDirectionToGastCommand?.RaiseCanExecuteChanged();
                SetPlayDirectionToHeimCommand?.RaiseCanExecuteChanged();
            }
        }
        public bool GameIsRunning
        {
            get => _gameIsRunning;
            set
            {
                _gameIsRunning = value;
                RaisePropertyChanged(nameof(LockTeams));
            }
        }
        public string ErrorFeed
        {
            get => _errorFeed;
            set
            {
                _errorFeed = value;
                RaisePropertyChanged(nameof(ErrorFeed));
            }
        }


        public bool LockTeams { get => !GameIsRunning; }

        public VirtualDisplayViewModel VirtualDisplayViewModel { get; set; }
        public PlayersForGameViewModel HeimPlayersForGameViewModel { get; set; }
        public PlayersForGameViewModel GastPlayersForGameViewModel { get; set; }



        public GameViewModel(Sport selectedSport)
        {
            SelectedSport = selectedSport;

            VirtualDisplayViewModel = _virtualDisplayViewModel;

            HeimPlayersForGameViewModel = new PlayersForGameViewModel(selectedSport, true)
            {
                Players = SelectedHeimTeam?.TeamMember
            };

            GastPlayersForGameViewModel = new PlayersForGameViewModel(selectedSport, false)
            {
                Players = SelectedGastTeam?.TeamMember
            };



            SetPlayDirectionToHeimCommand = new RelayCommand(() => ExecuteSetPlayDirectionToHeimCommand(), CanExecuteSetPlayDirectionToHeimCommand);
            SetPlayDirectionToGastCommand = new RelayCommand(() => ExecuteSetPlayDirectionToGastCommand(), CanExecuteSetPlayDirectionToGastCommand);
            SendHeimTeamCommand = new RelayCommand(() => ExecuteSendHeimDataCommand(), CanExecuteSendHeimDataCommand);
            SendGastTeamCommand = new RelayCommand(() => ExecuteSendGastDataCommand(), CanExecuteSendGastDataCommand);

            Messenger.Default.Register<IsAktivePlayerChangedMessage>(this, (action) => CheckAktivePlayerAmountOfTeam());
            Messenger.Default.Register<SportChangedMessage>(this, (action) => SelectedSport = action.Sport);
            Messenger.Default.Register<GameRelevantInfoChangedMessage>(this, (action) => SaveGameIfGameRuns());
            Messenger.Default.Register<LoadGameMessage>(this, (action) => LoadGame(action));
            Messenger.Default.Register<PeriodeAndGameDirectionReceivedMessage>(this, (action) => PlayDirection = action.PlayDirection);
            Messenger.Default.Register<ErrorFeedMessage>(this, (action) => ErrorFeed += string.Format("{0}\n", action.ErrorMessage));
            Messenger.Default.Register<PlayerReceivedMessage>(this, (action) => MapPlayer(action));

            ConnectionInfo = new ConnectionInfo();


            if (Properties.Settings.Default.SoftwareUnlocked)
                CommunicationManager = new CommunicationManager(ConnectionInfo);
        }

        private void MapPlayer(PlayerReceivedMessage action)
        {
            if (action.TeamTyp == TeamTyp.Heim)
            {
                if (SelectedHeimTeam != null)
                {
                    SelectedHeimTeam.TeamMember[action.PositionIndex].Update(action);
                }
            }
            else if (action.TeamTyp == TeamTyp.Gast)
            {
                if (SelectedGastTeam != null)
                {
                    SelectedGastTeam.TeamMember[action.PositionIndex].Update(action);
                }
            }
        }

        private bool CanExecuteSendGastDataCommand()
        {
            return SelectedGastTeam != null;
        }

        private void ExecuteSendGastDataCommand()
        {
            Messenger.Default.Send(new SendTeamsMessage(new List<Team>() { SelectedGastTeam }));
        }

        private bool CanExecuteSendHeimDataCommand()
        {
            return SelectedHeimTeam != null;
        }

        private void ExecuteSendHeimDataCommand()
        {
            Messenger.Default.Send(new SendTeamsMessage(new List<Team>() { SelectedHeimTeam }));
        }

        private bool CanExecuteSetPlayDirectionToGastCommand()
        {
            return PlayDirection == PlayDirection.Left;
        }

        private void ExecuteSetPlayDirectionToGastCommand()
        {
            PlayDirection = PlayDirection.Right;
            Messenger.Default.Send(new PeriodeAndGameDirectionSendMessage(PlayDirection));
        }

        private void ExecuteSetPlayDirectionToHeimCommand()
        {
            PlayDirection = PlayDirection.Left;
            Messenger.Default.Send(new PeriodeAndGameDirectionSendMessage(PlayDirection));
        }

        private bool CanExecuteSetPlayDirectionToHeimCommand()
        {
            return PlayDirection == PlayDirection.Right;
        }

        private void SaveGameIfGameRuns()
        {
            if (GameIsRunning)
                TeamLoaderSaver.SaveCurrentGame(SelectedHeimTeam, SelectedGastTeam);
        }

        private void LoadGame(LoadGameMessage action)
        {
            try
            {
                SelectSavedSport(action.Heim.KindOfSport);

                ObservableCollection<Team> teams = TeamLoaderSaver.LoadTeams(SelectedSport.KindOfSport);

                UpdateTeamsListWithLoadedTeams(teams, action.Heim);
                UpdateTeamsListWithLoadedTeams(teams, action.Gast);

                LoadTeams(teams, action.Heim.DisplayName, action.Gast.DisplayName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Das Spiel konnte leider nicht geladen werden. \n Exception: {0} \n InnerException{1}", ex.Message, ex.InnerException));
            }
        }

        private void UpdateTeamsListWithLoadedTeams(ObservableCollection<Team> teams, Team loadedTeam)
        {
            if (teams.Any(x => x.DisplayName == loadedTeam.DisplayName))
                teams.Where(x => x.DisplayName == loadedTeam.DisplayName).First().TeamMember = loadedTeam.TeamMember;
            else
                teams.Add(loadedTeam);
        }

        private void SelectSavedSport(KindOfSport sport)
        {
            if (SelectedSport.KindOfSport != sport)
                Messenger.Default.Send(new ChangeSportMessage(sport));
        }

        private void CheckAktivePlayerAmountOfTeam()
        {
            HeimPlayersForGameViewModel?.IsAktivePlayerChanged(SelectedSport.MaxAktivePlayer);
            GastPlayersForGameViewModel?.IsAktivePlayerChanged(SelectedSport.MaxAktivePlayer);
        }

        public void LoadDefaultTeams()
        {
            HeimTeams = null;
            GastTeams = null;

            SelectedGastTeam = null;
            SelectedHeimTeam = null;

            HeimTeams = TeamLoaderSaver.LoadTeams(SelectedSport.KindOfSport);
            GastTeams = TeamLoaderSaver.LoadTeams(SelectedSport.KindOfSport);

            if (HeimTeams.Count >= 2)
            {
                SelectedHeimTeam = HeimTeams[0];
                SelectedGastTeam = GastTeams[0];
            }
            else if (HeimTeams.Count > 0)
            {
                SelectedHeimTeam = HeimTeams[0];
                SelectedGastTeam = null;
            }
            else
            {
                SelectedGastTeam = null;
                SelectedHeimTeam = null;
            }
        }

        public void LoadTeams(ObservableCollection<Team> teams, string heimTeamDisplayName, string gastTeamDisplayName)
        {
            ObservableCollection<Team> teamsGast = new ObservableCollection<Team>();
            foreach (Team team in teams)
            {
                teamsGast.Add((Team)team.Clone());
            }

            HeimTeams = teams;
            GastTeams = teamsGast;

            SelectedHeimTeam = HeimTeams.Where(x => x.DisplayName == heimTeamDisplayName).First();
            SelectedGastTeam = GastTeams.Where(x => x.DisplayName == gastTeamDisplayName).First();
        }

        internal void ResetTeams()
        {
            SelectedHeimTeam.Reset();
            SelectedGastTeam.Reset();
        }

        private void SetTeamTyp(Team team, TeamTyp teamTyp)
        {
            if (team == null)
                return;
            foreach (Player player in team.TeamMember)
                player.TeamTyp = teamTyp;
        }
    }
}
