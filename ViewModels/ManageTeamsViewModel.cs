﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using S_eMotion_TEAMS.Entities;
using S_eMotion_TEAMS.Messenges;
using S_eMotion_TEAMS.Model.Entities;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace S_eMotion_TEAMS.ViewModels
{
    public class ManageTeamsViewModel : ViewModelBase
    {
        readonly static EditSquadViewModel _editSquadViewModel = new EditSquadViewModel();
        readonly static EditTeamViewModel _editTeamViewModel = new EditTeamViewModel();


        private ObservableCollection<Team> _teams;
        private Team _selectedTeam;
        private Sport _selectedSport;

        public RelayCommand AddTeamCommand { get; private set; }
        public RelayCommand DeleteTeamCommand { get; private set; }
        public RelayCommand MovePlayerToTeamCommand { get; private set; }
        public RelayCommand MovePlayerToSquadCommand { get; private set; }

        public EditSquadViewModel EditSquadViewModel { get; set; }
        public EditTeamViewModel EditTeamViewModel { get; set; }



        public Sport SelectedSport
        {
            get => _selectedSport;
            set
            {
                if (Teams != null)
                    TeamLoaderSaver.SaveTeams(Teams, SelectedSport.KindOfSport);
                _selectedSport = value;
                RaisePropertyChanged(nameof(SelectedSport));
                Teams = TeamLoaderSaver.LoadTeams(SelectedSport.KindOfSport);
                SelectedTeam = Teams[0];
            }
        }

        public Team SelectedTeam
        {
            get => _selectedTeam;
            set
            {
                _selectedTeam = value;
                RaisePropertyChanged(nameof(SelectedTeam));
                _editSquadViewModel.Players = _selectedTeam?.Squad;
                _editTeamViewModel.Players = _selectedTeam?.TeamMember;
                _editTeamViewModel.SelectedPlayer = null;
                _editSquadViewModel.SelectedPlayer = null;
                SelectedTeam?.IsAktivePlayerChanged(SelectedSport.MaxAktivePlayer);
            }
        }

        public ObservableCollection<Team> Teams
        {
            get => _teams;
            set
            {
                _teams = value;
                RaisePropertyChanged(nameof(Teams));
                DeleteTeamCommand?.RaiseCanExecuteChanged();
            }
        }


        public ManageTeamsViewModel(Sport selectedSport)
        {
            SelectedSport = selectedSport;

            Teams = TeamLoaderSaver.LoadTeams(SelectedSport.KindOfSport);

            MovePlayerToTeamCommand = new RelayCommand(() => MovePlayerToTeam(), CanPlayerMoveToTeam);
            MovePlayerToSquadCommand = new RelayCommand(() => MovePlayerToSquad(), CanPlayerMoveToSquad);
            AddTeamCommand = new RelayCommand(() => AddTeam());
            DeleteTeamCommand = new RelayCommand(() => DeleteTeam(), CanDeleteTeam);

            _editSquadViewModel.SelectedSport = SelectedSport;
            _editTeamViewModel.SelectedSport = SelectedSport;

            EditSquadViewModel = _editSquadViewModel;
            EditTeamViewModel = _editTeamViewModel;

            SelectedTeam = Teams[0];

            Messenger.Default.Register<CanMovePlayerToSquadMessage>(this, (action) => MovePlayerToSquadCommand.RaiseCanExecuteChanged());
            Messenger.Default.Register<CanMovePlayerToTeamMessage>(this, (action) => MovePlayerToTeamCommand.RaiseCanExecuteChanged());
            Messenger.Default.Register<MaxSportTeamSizeChangedMessage>(this, (action) => MaxSportTeamSizeChanged());
            Messenger.Default.Register<MaxSportAktivePlayerChangedMessage>(this, (action) => MaxSportAktivePlayerChanged());
            Messenger.Default.Register<IsAktivePlayerChangedMessage>(this, (action) => SelectedTeam?.IsAktivePlayerChanged(SelectedSport.MaxAktivePlayer));
            Messenger.Default.Register<SportChangedMessage>(this, (action) => SelectedSport = action.Sport);
        }

        private void DeleteTeam()
        {
            var team = SelectedTeam;
            int index = Teams.IndexOf(team);

            if (index > 0)
                index -= 1;

            Teams.Remove(team);

            SelectedTeam = Teams[index];

        }

        private void MaxSportAktivePlayerChanged()
        {
            if (_editTeamViewModel.Players.Where(x => x.IsAktive == true).ToList().Count() > SelectedSport?.MaxAktivePlayer)
            {
                for (int i = _editTeamViewModel.Players.Count - 1; i >= 0; i--)
                {
                    if (_editTeamViewModel.Players[i].IsAktive)
                    {
                        _editTeamViewModel.Players[i].IsAktive = false;
                        if (_editTeamViewModel.Players.Where(x => x.IsAktive == true).ToList().Count() <= SelectedSport.MaxAktivePlayer)
                        {
                            SelectedTeam.IsAktivePlayerChanged(SelectedSport.MaxAktivePlayer);
                            return;
                        }
                    }
                }
            }
        }

        private void MaxSportTeamSizeChanged()
        {
            if (_editTeamViewModel.Players?.Count > SelectedSport?.TeamSize)
            {
                for (int i = _editTeamViewModel.Players.Count - 1; i >= SelectedSport.TeamSize; i--)
                {
                    var player = _editTeamViewModel.Players[i];
                    _editTeamViewModel.Players.Remove(player);
                    if (_editTeamViewModel.SelectedPlayer == player)
                        _editTeamViewModel.SelectedPlayer = _editTeamViewModel.Players[i - 1];
                    _editSquadViewModel.Players.Insert(0, player);
                }
            }
            MovePlayerToTeamCommand.RaiseCanExecuteChanged();
        }

        private void MovePlayerToSquad()
        {
            var player = _editTeamViewModel.SelectedPlayer;
            _editTeamViewModel.SelectedPlayer = null;
            _editTeamViewModel.Players.Remove(player);
            player.IsAktive = false;
            _editSquadViewModel.Players.Insert(_editSquadViewModel.Players.Count, player);
            _editSquadViewModel.SelectedPlayer = player;
        }

        public bool CanPlayerMoveToSquad()
        {
            return _editTeamViewModel.SelectedPlayer != null;
        }
        private bool CanPlayerMoveToTeam()
        {
            return _editTeamViewModel.Players?.Count < SelectedSport.TeamSize && _editSquadViewModel.SelectedPlayer != null;
        }
        private bool CanDeleteTeam()
        {
            return Teams.Count > 1;
        }

        private void AddTeam()
        {
            Teams.Insert(0, new Team()
            {
                DisplayName = "Neues Team",
                KindOfSport = SelectedSport.KindOfSport,
                TeamName = "Neues Team",
                Squad = new ObservableCollection<Player>(),
                TeamMember = new ObservableCollection<Player>()
            });

            SelectedTeam = Teams[0];
            DeleteTeamCommand.RaiseCanExecuteChanged();
        }

        public void MovePlayerToTeam()
        {
            var player = _editSquadViewModel.SelectedPlayer;
            _editSquadViewModel.SelectedPlayer = null;
            _editSquadViewModel.Players.Remove(player);
            _editTeamViewModel.Players.Insert(_editTeamViewModel.Players.Count, player);
            _editTeamViewModel.SelectedPlayer = player;
        }

    }
}
