using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using S_eMotion_TEAMS.ControlPanel;
using S_eMotion_TEAMS.Entities;
using S_eMotion_TEAMS.Messenges;
using S_eMotion_TEAMS.Model.Entities;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Windows.Markup;
using System.Windows.Navigation;

namespace S_eMotion_TEAMS.ViewModels
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        /// 

        private ViewModelBase _currentViewModel;
        private string _selectedSportAsString = nameof(Basketball);
        private bool _gameModeOn;
        private string _pageTitle;
        
        readonly static List<Sport> _sports = new List<Sport>() { new Basketball(), new Handball(), new CustomSport() };

        public string SelectedSportAsString
        {
            get => _selectedSportAsString;
            set
            {
                if (value == null)
                    return;
                _selectedSportAsString = value;
                RaisePropertyChanged(nameof(SelectedSportAsString));
                Messenger.Default.Send(new SportChangedMessage(SelectedSport));
            }
        }

        public Sport SelectedSport { get => Sports.Where(x => x.KindOfSport.ToString() == SelectedSportAsString).FirstOrDefault(); }

        public List<Sport> Sports
        {
            get => _sports;
            set
            {
                RaisePropertyChanged(nameof(Sports));
            }
        }

        public ViewModelBase CurrentViewModel
        {
            get
            {
                return _currentViewModel;
            }
            set
            {
                if (_currentViewModel == value)
                    return;

                else if (_currentViewModel?.GetType() == typeof(ManageTeamsViewModel))
                {
                    TeamLoaderSaver.SaveTeams((_currentViewModel as ManageTeamsViewModel).Teams, SelectedSport.KindOfSport);
                    if (value?.GetType() == typeof(GameViewModel))
                        (value as GameViewModel).LoadDefaultTeams();
                }
                else if (_currentViewModel?.GetType() == typeof(OptionsViewModel))
                {
                    Properties.Settings.Default.Save();
                }

                if (value?.GetType() != typeof(GameViewModel))
                {
                    IsGameModeOn = false;
                }

                _currentViewModel = value;
                RaisePropertyChanged(nameof(CurrentViewModel));
                RaisePropertyChanged(nameof(IsGameModeOn));
                RaisePropertyChanged(nameof(CanSetGameMode));
                RaisePropertyChanged(nameof(PageTitle));
                SetGameModeCommand?.RaiseCanExecuteChanged();
                LoadGameCommand?.RaiseCanExecuteChanged();
                ResetTeamsCommand?.RaiseCanExecuteChanged();
            }
        }

        public string PageTitle
        {
            get
            {
                if (CurrentViewModel.GetType() == typeof(GameViewModel))
                    return "S-eMotion TEAMS";
                else if (CurrentViewModel.GetType() == typeof(ManageTeamsViewModel))
                    return "Manage Teams";
                else if (CurrentViewModel.GetType() == typeof(OptionsViewModel))
                    return "Options";
                else
                    return "S-eMotion TEAMS";
            }

            set
            {
                _pageTitle = value;
            }
        }

        public bool IsGameModeOn
        {
            get => _gameModeOn;
            set
            {
                if (_gameModeOn == value)
                    return;

                _gameModeOn = value;
                RaisePropertyChanged(nameof(IsGameModeOn));
                RaisePropertyChanged(nameof(IsGameModeOff));
                LoadGameCommand?.RaiseCanExecuteChanged();

                if (CurrentViewModel.GetType() == typeof(GameViewModel))
                    (CurrentViewModel as GameViewModel).GameIsRunning = IsGameModeOn;
            }
        }

        public bool CanSetGameMode { get => CurrentViewModel.GetType() == typeof(GameViewModel); }

        public bool IsGameModeOff { get => !IsGameModeOn; }

        public RelayCommand OpenManageTeamsCommand { get; private set; }
        public RelayCommand OpenGameCommand { get; private set; }
        public RelayCommand OpenOptionsCommand { get; private set; }
        public RelayCommand LoadGameCommand { get; private set; }
        public RelayCommand SetGameModeCommand { get; private set; }
        public RelayCommand ResetTeamsCommand { get; private set; }
        public RelayCommand OpenLizenzMenuCommand { get; private set; }

        public MainViewModel()
        {
            OpenManageTeamsCommand = new RelayCommand(() => ExecuteManageTeamsCommand());
            OpenGameCommand = new RelayCommand(() => ExecuteOpenGameCommand());
            OpenOptionsCommand = new RelayCommand(() => ExecuteOpenOptionsCommand());
            LoadGameCommand = new RelayCommand(() => ExecuteLoadGameCommand(), CanExecuteLoadGameCommand);
            ResetTeamsCommand = new RelayCommand(() => ExecuteResetTeamsCommand(), CanExecuteResetTeamsCommand);
            OpenLizenzMenuCommand = new RelayCommand(() => ExecuteOpenLizenzMenuCommand());

            Messenger.Default.Register<ChangeSportMessage>(this, (action) => LoadGame(action));
            Messenger.Default.Register<LicenceViewModelFinishedMessage>(this, (action) => UnlockSoftware());

            PrecheckLicence();
        }

        private void PrecheckLicence()
        {
            if (string.IsNullOrWhiteSpace(Properties.Settings.Default.UserLicenceKey))
                CurrentViewModel = new LicenceViewModel();
            else if (new Licence(Properties.Settings.Default.UserLicenceKey).ValidateUserKey() == UserTypIsNotValidTyp.LicenceIsValid)
                CurrentViewModel = new GameViewModel(SelectedSport);
            else
                CurrentViewModel = new LicenceViewModel();
        }

        private void ExecuteOpenLizenzMenuCommand()
        {
            CurrentViewModel = new LicenceViewModel();
        }

        private void UnlockSoftware()
        {
            CurrentViewModel = new GameViewModel(SelectedSport);
        }

        private void ExecuteOpenOptionsCommand()
        {
            CurrentViewModel = new OptionsViewModel(SelectedSport);
        }

        private void ExecuteResetTeamsCommand()
        {
            var gameViewModel = (CurrentViewModel as GameViewModel);

            gameViewModel.ResetTeams();

            if (Properties.Settings.Default.SendCompleteTeams)
            {
                List<Team> teamsToSend = new List<Team>();

                if (gameViewModel.SelectedHeimTeam != null)
                    teamsToSend.Add(gameViewModel.SelectedHeimTeam);

                if (gameViewModel.SelectedGastTeam != null)
                    teamsToSend.Add(gameViewModel.SelectedGastTeam);

                if (teamsToSend.Count != 0)
                    Messenger.Default.Send(new SendTeamsMessage(teamsToSend));
            }
        }

        private bool CanExecuteResetTeamsCommand()
        {
            return (CurrentViewModel.GetType() == typeof(GameViewModel)) && IsGameModeOff;
        }

        private void LoadGame(ChangeSportMessage action)
        {
            SelectedSportAsString = action.NewSport.ToString();
        }

        private bool CanExecuteLoadGameCommand()
        {
            return (CurrentViewModel.GetType() == typeof(GameViewModel)) && IsGameModeOff;
        }

        private void ExecuteLoadGameCommand()
        {
            Messenger.Default.Send(new LoadGameMessage(TeamLoaderSaver.LoadGame()));
        }

        private void ExecuteManageTeamsCommand()
        {
            CurrentViewModel = new ManageTeamsViewModel(SelectedSport);
        }

        private void ExecuteOpenGameCommand()
        {
            CurrentViewModel = new GameViewModel(SelectedSport);
        }

    }
}