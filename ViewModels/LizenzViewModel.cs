﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using QRCoder;
using S_eMotion_TEAMS.Messenges;
using S_eMotion_TEAMS.Model.Entities;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace S_eMotion_TEAMS.ViewModels
{
    public class LicenceViewModel : ViewModelBase
    {
        private const string AUTHENTICATION_LINK_BASE = "https://lizenz-sport.schauf.eu/s-eMotion_license/auth/";

        private string _masterLicenceKey;
        private Licence _licence;
        private string _userLicenceKey;
        private Licence _userLicence;
        private string _licenceError;

        private bool _showManuellRegistration;
        public bool UserLicenceInputIsEnabled
        {
            get => userLicenceIsEnabled;
            private set
            {
                userLicenceIsEnabled = value;
                RaisePropertyChanged(nameof(UserLicenceInputIsEnabled));
            }
        }

        public RelayCommand ContinueWithoutLicenceCommand { get; private set; }
        public RelayCommand SaveUserLicenceCommand { get; private set; }
        public RelayCommand GenareteUserLicenceCommand { get; private set; }
        public RelayCommand DeleteUserLicenceCommand { get; private set; }
        public RelayCommand EditUserLicenceCommand { get; private set; }

        public string AuthenticationLink
        {
            get
            {
                if (MasterLicence != null)
                    return string.Format("{0}{1}", AUTHENTICATION_LINK_BASE, MasterLicence.GetAuthenticationCode());
                else
                    return string.Empty;
            }
        }
        public int SchaufIconColumnSpan
        {
            get
            {
                if (ShowManuellRegistration)
                    return 1;
                else
                    return 2;
            }
        }
        public bool ShowManuellRegistration
        {
            get { return _showManuellRegistration; }
            set
            {
                _showManuellRegistration = value;
                RaisePropertyChanged(nameof(ShowManuellRegistration));
                RaisePropertyChanged(nameof(SchaufIconColumnSpan));
                RaisePropertyChanged(nameof(AuthenticationLink));
            }
        }
        private BitmapImage _QRCode;
        private bool userLicenceIsEnabled;

        public BitmapImage QRCode
        {
            get { return _QRCode; }
            set
            {
                _QRCode = value;
                RaisePropertyChanged(nameof(QRCode));
            }
        }



        public string MasterLicenceKey
        {
            get => _masterLicenceKey;

            set
            {
                if (!Regex.IsMatch(value, "^([A-Za-z0-9]{7}[-]){5}[A-Za-z0-9]{7}$"))
                    LicenceError = "Das eingegebene Format der Licence ist ungültig!";
                else
                {
                    MasterLicence = new Licence(value);
                }

                _masterLicenceKey = value;
            }
        }
        public Licence MasterLicence
        {
            get => _licence;
            set
            {
                _licence = value;

                GenareteUserLicenceCommand.RaiseCanExecuteChanged();

                if (!MasterLicence.IsValid)
                {
                    LicenceError = "Die Licence ist Fehlerhaft.";
                }
            }
        }

        public string UserLicencKey
        {
            get => _userLicenceKey;

            set
            {
                if (string.IsNullOrWhiteSpace(value))
                { }
                else if (!Regex.IsMatch(value, "^([A-Za-z0-9]{7}[-]){5}[A-Za-z0-9]{7}$"))
                    LicenceError = "Das Format der Benutzerlizenz ist ungültig!";
                else
                {
                    UserLicence = new Licence(value);
                }

                _userLicenceKey = value;
                RaisePropertyChanged(nameof(UserLicencKey));
            }
        }
        public Licence UserLicence
        {
            get => _userLicence;
            set
            {
                _userLicence = value;

                if (value == null)
                    return;

                RaisePropertyChanged(nameof(LicenceError));
                RaisePropertyChanged(nameof(UserLicence));
                DeleteUserLicenceCommand.RaiseCanExecuteChanged();
                ContinueWithoutLicenceCommand.RaiseCanExecuteChanged();
                CheckUserKey();
                SaveUserLicenceCommand.RaiseCanExecuteChanged();
            }
        }

        public string LicenceError
        {
            get => _licenceError;
            set
            {
                _licenceError = value;
                RaisePropertyChanged(nameof(LicenceError));
            }
        }


        public LicenceViewModel()
        {
            LicenceError = string.Empty;

            GenareteUserLicenceCommand = new RelayCommand(() => ExecuteGenerateUserLicenceCommand());
            EditUserLicenceCommand = new RelayCommand(() => ExecuteEditUserLicenceCommand());
            DeleteUserLicenceCommand = new RelayCommand(() => ExecuteDeleteUserLicenceCommand(), CanExecuteDeleteUserLicenceCommand);
            SaveUserLicenceCommand = new RelayCommand(() => ExecuteSaveUserLicenceCommand(), CanExecuteSaveUserLicenceCommand);
            ContinueWithoutLicenceCommand = new RelayCommand(() => ExecuteContinueWithoutLicenceCommand(), CanExecuteContinueWithoutLicenceCommand);


            if (!string.IsNullOrWhiteSpace(Properties.Settings.Default.UserLicenceKey))
                UserLicencKey = Properties.Settings.Default.UserLicenceKey;
        }

        private bool CanExecuteContinueWithoutLicenceCommand()
        {
            if (UserLicence != null)
                return !(UserLicence.ValidateUserKey() == UserTypIsNotValidTyp.LicenceIsValid);
            else
                return true;
        }

        private void ExecuteEditUserLicenceCommand()
        {
            UserLicenceInputIsEnabled = !UserLicenceInputIsEnabled;
        }

        private void ExecuteDeleteUserLicenceCommand()
        {
            UserLicence = null;
            UserLicencKey = string.Empty;

            Properties.Settings.Default.UserLicenceKey = string.Empty;
            Properties.Settings.Default.SoftwareUnlocked = false;
            Properties.Settings.Default.Save();

            DeleteUserLicenceCommand.RaiseCanExecuteChanged();
            SaveUserLicenceCommand.RaiseCanExecuteChanged();
            ContinueWithoutLicenceCommand.RaiseCanExecuteChanged();
        }

        private bool CanExecuteDeleteUserLicenceCommand()
        {
            return UserLicence != null;
        }

        private void ExecuteSaveUserLicenceCommand()
        {
            if (UserLicence.ValidateUserKey() == UserTypIsNotValidTyp.LicenceIsValid)
            {
                Properties.Settings.Default.SoftwareUnlocked = true;
                Properties.Settings.Default.UserLicenceKey = UserLicencKey;
                Properties.Settings.Default.Save();
            }

            SaveUserLicenceCommand.RaiseCanExecuteChanged();
        }

        private bool CanExecuteSaveUserLicenceCommand()
        {
            if (UserLicence != null)
            {
                if (Properties.Settings.Default.UserLicenceKey == UserLicencKey && string.IsNullOrWhiteSpace(UserLicencKey) && string.IsNullOrWhiteSpace(Properties.Settings.Default.UserLicenceKey))
                    return true;
                else
                    return (UserLicence.IsValid) && (UserLicence.ValidateUserKey() == UserTypIsNotValidTyp.LicenceIsValid) && (Properties.Settings.Default.UserLicenceKey != UserLicencKey);
            }
            else
                return false;
        }

        private void CheckUserKey()
        {
            switch (UserLicence.ValidateUserKey())
            {
                case UserTypIsNotValidTyp.LicenceExpired:
                    LicenceError = string.Format("Die Benutzerlizenz ist ausgelaufen. Gültig bis: {0}", UserLicence.GetEndDate());
                    break;
                case UserTypIsNotValidTyp.LicenceIsNotValid:
                    LicenceError = "Die Benutzerlizenz ist nicht mehr Gültig";
                    break;
                case UserTypIsNotValidTyp.LicenceIsNotValidForVersion:
                    LicenceError = string.Format("Die Benutzerlizenz ist nicht mit dieser Softwareverison gültig. Letzte gültige Version: {0}", UserLicence.GetEndVersion());
                    break;
                case UserTypIsNotValidTyp.LicenceIsNoUserLicence:
                    LicenceError = "Die Lizenz ist keine Benutzerlizenz.";
                    break;
                case UserTypIsNotValidTyp.SystemDoesNotMatch:
                    LicenceError = "Das System auf dem die Software läuft ist ein Anderes als das in der Lizenz angegebene.";
                    break;
                case UserTypIsNotValidTyp.LicenceIsValid:
                    LicenceError = "Die Benutzerlizenz ist gültig.";
                    return;
            }
            Properties.Settings.Default.SoftwareUnlocked = false;
        }

        private void ExecuteGenerateUserLicenceCommand()
        {
            SendAuthenticationCode();
        }

        private async void SendAuthenticationCode()
        {
            try
            {
                if (MasterLicence == null)
                {
                    LicenceError = "Zum generieren der Benutzerlizenz muss ein gültiger Lizenzkey angegeben werden.";
                    return;
                }
                if (!MasterLicence.IsValid)
                {
                    LicenceError = "Der Lizenzkey ist ungültig";
                    return;
                }
                if (MasterLicence.LicenceType == LicenceType.User)
                {
                    LicenceError = "Der Lizenzkey ist eine Benutzerlizenz.";
                    return;
                }

                using (var httpClient = new HttpClient())
                {
                    HttpResponseMessage httpResponse = await httpClient.PostAsync(string.Format("{0}{1}", AUTHENTICATION_LINK_BASE, MasterLicence.GetAuthenticationCode()), new StringContent(string.Empty));

                    if (httpResponse.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        LicenceError = string.Format("Beim senden trat ein fehler auf, Sie können mit dem QR-Code oder dem Link die Lizenz manuell registrieren. HttpStatusCode: {0}", httpResponse.StatusCode);
                        EnableManuelRegistration();
                        return;
                    }

                    var response = httpResponse.Content.ReadAsStringAsync();

                    if (Regex.IsMatch(response.Result, "^([A-Za-z0-9]{7}[-]){5}[A-Za-z0-9]{7}$"))
                    {
                        UserLicencKey = response.Result;
                    }
                    else
                    {
                        LicenceError = response.Result;
                    }
                }
            }
            catch (Exception ex)
            {
                LicenceError = string.Format("Beim Prüfen der Lizenz ist ein Fehler aufgetreten, bitte wenden Sie sich an den Kunden Support oder melden registrieren Sie die Lizenz manuell mit dem QR-Code oder dem Link\n. Fehler: {0}, FehlerDetails: {1}", ex.Message, ex.InnerException);
                EnableManuelRegistration();
            }

        }

        private void EnableManuelRegistration()
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(string.Format("{0}{1}", AUTHENTICATION_LINK_BASE, MasterLicence.GetAuthenticationCode()), QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);

            using (MemoryStream memory = new MemoryStream())
            {
                qrCode.GetGraphic(20).Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                QRCode = bitmapimage;
            }

            ShowManuellRegistration = true;
        }

        private void ExecuteContinueWithoutLicenceCommand()
        {
            Properties.Settings.Default.SoftwareUnlocked = false;
            Properties.Settings.Default.Save();
            Messenger.Default.Send(new LicenceViewModelFinishedMessage());
        }
    }
}
