﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelReceivedMesseges;
using S_eMotion_TEAMS.Model.Entities;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.ViewModels
{
    class VirtualDisplayViewModel : ViewModelBase
    {
        private GameTimeState gameTimeState;
        private DateTime time;
        private string timeSeparator;
        private bool _showMilliSeconds;

        public int ScoreHeim { get; set; }
        public int ScoreGast { get; set; }
        public int Period { get; set; }
        public DateTime Time
        {
            get => time;
            set
            {
                time = value;

                RaisePropertyChanged(nameof(TimeInFrontOfTimeSeperator));
                RaisePropertyChanged(nameof(TimeAfterTimeSeperator));
            }
        }
        public int TimeInFrontOfTimeSeperator
        {
            get
            {
                if (ShowMilliSeconds)
                    return Time.Second;
                else
                    return Time.Minute;
            }
        }
        public int TimeAfterTimeSeperator
        {
            get
            {
                if (ShowMilliSeconds)
                    return Time.Millisecond;
                else
                    return Time.Second;
            }
        }
        public string TimeSeparator
        {
            get => timeSeparator;
            set
            {
                timeSeparator = value;
                RaisePropertyChanged(nameof(TimeSeparator));
            }
        }

        public int FoulHeim { get; set; }
        public int FoulGast { get; set; }
        public GameTimeState GameTimeState
        {
            get => gameTimeState;
            set
            {
                gameTimeState = value;
                RaisePropertyChanged(nameof(ColonColor));
            }
        }
        public string ColonColor
        {
            get
            {
                switch (GameTimeState)
                {
                    case GameTimeState.Break:
                        return "#ffffff";
                    case GameTimeState.Running:
                        return "#2dff4a";
                    case GameTimeState.Stop:
                        return "#ff3333";
                    default:
                        return "#2dff4a";
                }
            }
        }
        public bool ShowMilliSeconds
        {
            get => _showMilliSeconds;
            set
            {
                _showMilliSeconds = value;

                if (_showMilliSeconds)
                {
                    timeSeparator = ".";
                }
                else
                {
                    timeSeparator = ":";
                }

                RaisePropertyChanged(nameof(TimeInFrontOfTimeSeperator));
                RaisePropertyChanged(nameof(TimeAfterTimeSeperator));
            }
        }

        public VirtualDisplayViewModel()
        {
            Messenger.Default.Register<GameTimeAndHornReceivedMessage>(this, (action) => SetTime(action));
            Messenger.Default.Register<PeriodeAndGameDirectionReceivedMessage>(this, (action) => Period = action.Period);
            Messenger.Default.Register<ScoreReceivedMessage>(this, (action) => SetScore(action));
            Messenger.Default.Register<TeamFoulsReceivedMessage>(this, (action) => SetFouls(action));

            var datetime = DateTime.Now;
            datetime = datetime.AddMilliseconds(1001 - datetime.Millisecond);

            ScoreHeim = 20;
            FoulHeim = 1;

            ScoreGast = 12;
            FoulGast = 4;

            GameTimeState = Model.Enums.GameTimeState.Stop;
            Period = 3;

            ShowMilliSeconds = false;

            Time = datetime;

        }

        private void SetTime(GameTimeAndHornReceivedMessage action)
        {
            Time = action.GameTime;

        }

        private void SetFouls(TeamFoulsReceivedMessage action)
        {
            FoulHeim = action.HeimFoul;
            FoulGast = action.GastFoul;
        }

        private void SetScore(ScoreReceivedMessage action)
        {
            ScoreHeim = action.ScoreHeim;
            ScoreGast = action.ScoreGast;
        }
    }
}
