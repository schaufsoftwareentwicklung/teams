﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using S_eMotion_TEAMS.Messenges;
using S_eMotion_TEAMS.Model.Entities;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace S_eMotion_TEAMS.ViewModels
{
    public class PlayersForGameViewModel : ViewModelBase
    {
        private Player _selectedPlayer;
        private ObservableCollection<Player> _players;
        private Sport _selectedSport;
        private string _columnNumbersToHide;
        private bool IsHeim;

        public ObservableCollection<Player> Players
        {
            get => _players;
            set
            {
                _players = value;
                RaisePropertyChanged(nameof(Players));
            }
        }

        public Player SelectedPlayer
        {
            get => _selectedPlayer;
            set
            {
                _selectedPlayer = value;
                RaisePropertyChanged(nameof(SelectedPlayer));
            }
        }

        public Sport SelectedSport
        {
            get => _selectedSport;
            set
            {
                _selectedSport = value;
                RaisePropertyChanged(nameof(BackgroundColor));
                RaisePropertyChanged(nameof(ShowBasketballInformation));
            }
        }

        public string BackgroundColor
        {
            get
            {
                if (IsHeim)
                    return SelectedSport.HeimColor;
                else
                    return SelectedSport.GastColor;
            }
        }

        public Visibility ShowBasketballInformation
        {
            get
            {
                switch (SelectedSport?.KindOfSport)
                {
                    case KindOfSport.Basketball:
                        ColumnNumbersToHide = string.Empty;
                        return Visibility.Visible;
                    case KindOfSport.Handball:
                        ColumnNumbersToHide = "3";
                        return Visibility.Hidden;
                    case KindOfSport.Benutzerdefiniert:
                        ColumnNumbersToHide = "3";
                        return Visibility.Hidden;
                    default:
                        ColumnNumbersToHide = "3";
                        return Visibility.Hidden;
                }
            }
        }

        public string ColumnNumbersToHide
        {
            get
            {
                return _columnNumbersToHide;
            }
            set
            {
                _columnNumbersToHide = value;
                RaisePropertyChanged(nameof(ColumnNumbersToHide));
            }
        }

        public PlayersForGameViewModel(Sport selectedSport, bool isHeim)
        {
            IsHeim = isHeim;
            SelectedSport = selectedSport;
            Messenger.Default.Register<SportChangedMessage>(this, (action) => SelectedSport = action.Sport);
        }

        public void IsAktivePlayerChanged(int maxAktivePlayer)
        {
            if (Players.Where(x => x.IsAktive == true).ToList().Count() < maxAktivePlayer)
                Players.Where(x => x.IsAktive == false).ToList().ForEach(x => x.IsAktiveEnabled = false);
            else
                Players.Where(x => x.IsAktive == false).ToList().ForEach(x => x.IsAktiveEnabled = true);
        }
    }
}
