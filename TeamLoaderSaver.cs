﻿using S_eMotion_TEAMS.Model.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
using System.IO;
using System.Text.Json;
using S_eMotion_TEAMS.Model.Enums;

namespace S_eMotion_TEAMS
{
    public static class TeamLoaderSaver
    {
        public static ObservableCollection<Team> LoadTeams(KindOfSport kindOfSport)
        {
            var teams = new ObservableCollection<Team>();
            string path = string.Format(@".\Teams\{0}", Enum.GetName(typeof(KindOfSport), kindOfSport));

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            FileInfo[] fileInfos = directoryInfo.GetFiles();

            if (fileInfos.Length == 0)
                teams.Add(GetNewTeam(kindOfSport));
            else
            {
                foreach (FileInfo file in fileInfos)
                    teams.Add(JsonSerializer.Deserialize<Team>(File.ReadAllText(file.FullName)));
            }

            return teams;
        }

        private static Team GetNewTeam(KindOfSport kindOfSport)
        {
            return new Team()
            {
                TeamName = "Neues Team",
                DisplayName = "Neues Team",
                KindOfSport = kindOfSport,
                Squad = new ObservableCollection<Player>(),
                TeamMember = new ObservableCollection<Player>()
            };
        }

        public static void SaveTeams(ObservableCollection<Team> teams, KindOfSport kindOfSport)
        {
            foreach (Team team in teams)
            {
                SaveTeam(team);
            }

            foreach (FileInfo file in new DirectoryInfo(string.Format(@".\Teams\{0}", Enum.GetName(typeof(KindOfSport), kindOfSport))).GetFiles("*.json", SearchOption.AllDirectories))
            {
                if (!teams.Contains(teams.Where(x => x.TeamName == file.Name.Replace(".json", "")).FirstOrDefault()))
                    File.Delete(file.FullName);
            }
        }

        private static void SaveTeam(Team team)
        {
            File.WriteAllText(string.Format(@".\Teams\{0}\{1}.json", Enum.GetName(typeof(KindOfSport), team.KindOfSport), team.TeamName), JsonSerializer.Serialize(team));
        }

        internal static void SaveCurrentGame(Team heim, Team gast)
        {
            if (!Directory.Exists(@".\CurrentGame\"))
                Directory.CreateDirectory(@".\CurrentGame\");

            File.WriteAllText(string.Format(@".\CurrentGame\Heim.json"), JsonSerializer.Serialize(heim));
            File.WriteAllText(string.Format(@".\CurrentGame\Gast.json"), JsonSerializer.Serialize(gast));
        }

        public static ObservableCollection<Team> LoadGame()
        {
            var teams = new ObservableCollection<Team>();
            string path = string.Format(@".\CurrentGame\");

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            FileInfo[] fileInfos = directoryInfo.GetFiles();

            if (fileInfos.Length == 0)
                return null;
            else
            {
                foreach (FileInfo file in fileInfos)
                {
                    if (file.Name == "Heim.json")
                        teams.Insert(0, JsonSerializer.Deserialize<Team>(File.ReadAllText(file.FullName)));
                    else if (file.Name == "Gast.json")
                        teams.Add(JsonSerializer.Deserialize<Team>(File.ReadAllText(file.FullName)));

                }
            }

            return teams;
        }
    }
}
