﻿using S_eMotion_TEAMS.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges
{
    public class SendTeamsMessage
    {
        public List<Team> TeamsToSend { get; private set; }

        public SendTeamsMessage(List<Team> teamsToSend)
        {
            TeamsToSend = teamsToSend;
        }
    }
}
