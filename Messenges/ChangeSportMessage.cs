﻿using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges
{
    class ChangeSportMessage
    {
        public KindOfSport NewSport { get; set; }
        public ChangeSportMessage(KindOfSport newSport)
        {
            NewSport = newSport;
        }
    }
}
