﻿using S_eMotion_TEAMS.Model.Entities;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges
{
    public class SportChangedMessage
    {
        public Sport Sport { get; set; }

        public SportChangedMessage(Sport selectedSport)
        {
            Sport = selectedSport;
        }
    }
}
