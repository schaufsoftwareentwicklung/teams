﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges
{
    public class ErrorFeedMessage
    {
        public string ErrorMessage { get; private set; }

        public ErrorFeedMessage( string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
    }
}
