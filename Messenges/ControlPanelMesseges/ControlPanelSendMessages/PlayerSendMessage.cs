﻿using S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll;
using S_eMotion_TEAMS.Model.Entities;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelSendMessages
{
    public class PlayerSendMessage : PlayerMessage
    {
        public PlayerSendMessage(byte[] payLoad, AdressByte adressByte) : base(payLoad, adressByte) { }

        public PlayerSendMessage(Player player) : base(player){}
    }
}
