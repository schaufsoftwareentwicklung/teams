﻿using S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll;
using S_eMotion_TEAMS.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelSendMessages
{
    public class PeriodeAndGameDirectionSendMessage : PeriodeAndGameDirectionMessage
    {
        public PeriodeAndGameDirectionSendMessage(byte[] payLoad) : base(payLoad)
        {
        }
        public PeriodeAndGameDirectionSendMessage(PlayDirection playDirection) : base(playDirection)
        {
        }
    }
}
