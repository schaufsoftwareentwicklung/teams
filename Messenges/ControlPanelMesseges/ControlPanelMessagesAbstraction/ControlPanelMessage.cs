﻿using S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelMessagesAbstraction
{
    public abstract class ControlPanelMessage : ByteConverter, IControlPanelMessage
    {
        public ControlPanelMessage(byte[] payLoad)
        {
        }

        protected ControlPanelMessage()
        {
        }

        public AdressByte AdressByte { get; set; }

        public AdressByte GetAdressByte()
        {
            return AdressByte;
        }

        public abstract byte[] GetPayLoad();
    }
}
