﻿using S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll;

namespace S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelMessagesAbstraction
{
    public interface IControlPanelMessage
    {
        AdressByte GetAdressByte();
        byte[] GetPayLoad();
    }
}
