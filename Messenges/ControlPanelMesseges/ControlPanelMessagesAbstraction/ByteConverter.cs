﻿using System;
using System.Collections.Generic;

namespace S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelMessagesAbstraction
{
    public class ByteConverter
    {
        public string ConvertByteToString(byte[] bytes)
        {
            string result = string.Empty;

            foreach (byte b in bytes)
                result += (char)b;

            return result;
        }

        public int ConvertByteToInt(byte[] bytes)
        {
            int result = 0;

            for (int i = 0; i < bytes.Length; i++)
            {
                result += bytes[i] * 10 ^ (bytes.Length - 1 - i);
            }

            return result;
        }

        public IEnumerable<byte> ConvertNumberToByte(int number)
        {
            var bytes = new List<byte>();

            foreach (char c in number.ToString())
                bytes.Add((byte)c);

            return bytes;
        }

        public byte[] GetSubArray(byte[] array, int startindex, int length)
        {
            var result = new byte[length];
            Array.Copy(array, startindex, result, 0, length);
            return result;
        }
    }
}
