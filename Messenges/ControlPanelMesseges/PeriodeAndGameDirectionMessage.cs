﻿using S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelMessagesAbstraction;
using S_eMotion_TEAMS.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges.ControlPanelMesseges
{
    public abstract class PeriodeAndGameDirectionMessage : ControlPanelMessage
    {
        public int Period { get; set; }
        public PlayDirection PlayDirection { get; set; }

        public PeriodeAndGameDirectionMessage(byte[] payLoad) : base(payLoad)
        {
            PlayDirection = (PlayDirection)payLoad[1];
            Period = ConvertByteToInt(GetSubArray(payLoad, 0, 1));
            AdressByte = AdressByte.PeriodeAndGameDirection;
        }

        public PeriodeAndGameDirectionMessage(PlayDirection playDirection) : base()
        {
            PlayDirection = PlayDirection;
            Period = 0;
            AdressByte = AdressByte.PeriodeAndGameDirection;
        }

        public override byte[] GetPayLoad()
        {
            var payLoad = new List<byte>();

            payLoad.Add(0x00);
            payLoad.Add((byte)PlayDirection);
            payLoad.Add(0x00);
            payLoad.Add(0x00);

            return payLoad.ToArray();
        }
    }
}
