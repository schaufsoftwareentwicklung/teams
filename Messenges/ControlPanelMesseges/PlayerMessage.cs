﻿using S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelMessagesAbstraction;
using S_eMotion_TEAMS.Model.Entities;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges.ControlPanelMesseges
{
    public abstract class PlayerMessage : ControlPanelMessage
    {
        public bool ValidFlag { get; set; }
        public int Number { get; set; }
        public int Foul { get; set; }
        public int Score { get; set; }
        public bool IsAktive { get; set; }
        public int PositionIndex { get; set; }
        public string Name { get; set; }
        public TeamTyp TeamTyp { get; set; }

        public PlayerMessage(byte[] payLoad, AdressByte adressByte) : base(payLoad)
        {
            this.ValidFlag = ConvertByteToInt(GetSubArray(payLoad, 0, 1)) != 1;
            this.Number = ConvertByteToInt(GetSubArray( payLoad,1, 2));
            this.Foul = ConvertByteToInt(GetSubArray(payLoad, 3, 1));
            this.Score = ConvertByteToInt(GetSubArray(payLoad, 4, 2));
            this.IsAktive = payLoad[6] == 0x00 ? true : false;
            this.PositionIndex = ConvertByteToInt(GetSubArray(payLoad, 7, 2));
            this.Name = ConvertByteToString(GetSubArray(payLoad, 9, 8));

            AdressByte = adressByte;
        }

        public PlayerMessage(Player player) : base()
        {
            this.ValidFlag = player.IsLatestFouler;
            this.Number = player.Number;
            this.Foul = player.Foul;
            this.Score = player.Score;
            this.IsAktive = player.IsAktive;
            this.PositionIndex = player.PositionIndex;
            this.Name = player.Name;

            AdressByte = (AdressByte)player.TeamTyp;
        }

        public override byte[] GetPayLoad()
        {
            var payLoad = new List<byte>();

            payLoad.Add(0x00);
            payLoad.AddRange(ConvertNumberToByte(this.Number));
            payLoad.AddRange(ConvertNumberToByte(this.Foul));
            payLoad.AddRange(ConvertNumberToByte(this.Score));
            payLoad.Add(this.IsAktive ? (byte)0x00 : (byte)0x01);
            payLoad.AddRange(ConvertNumberToByte(this.PositionIndex));

            foreach (char c in this.Name)
                payLoad.Add((byte)c);

            return payLoad.ToArray();
        }
    }
}
