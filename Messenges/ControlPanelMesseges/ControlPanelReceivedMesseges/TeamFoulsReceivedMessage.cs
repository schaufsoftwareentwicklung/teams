﻿using S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelReceivedMesseges
{
    public class TeamFoulsReceivedMessage : TeamFoulsMessage
    {
        public TeamFoulsReceivedMessage(byte[] payLoad) : base(payLoad)
        {
        }
    }
}
