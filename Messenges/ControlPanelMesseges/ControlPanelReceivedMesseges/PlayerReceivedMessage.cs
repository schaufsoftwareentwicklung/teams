﻿using S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll;
using S_eMotion_TEAMS.Model.Entities;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelReceivedMesseges
{
    public class PlayerReceivedMessage : PlayerMessage
    {
        public PlayerReceivedMessage(byte[] payLoad, AdressByte adressByte) : base(payLoad, adressByte) { }

        public PlayerReceivedMessage(Player player) : base(player){}
    }
}
