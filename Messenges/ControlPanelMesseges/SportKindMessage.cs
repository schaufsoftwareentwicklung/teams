﻿using S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelMessagesAbstraction;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges.ControlPanelMesseges
{
    public abstract class SportKindMessage : ControlPanelMessage
    {
        public KindOfSport KindOfSport { get; set; }

        public SportKindMessage(byte[] payLoad) : base(payLoad)
        {
            this.KindOfSport = ConvertToSport(payLoad[0], payLoad[1]);
        }

        private KindOfSport ConvertToSport(byte payLoad1, byte payLoad2)
        {
            if (payLoad1 != 0x30)
                return KindOfSport.Benutzerdefiniert;

            switch (payLoad2)
                {
                    case 0x31:
                        return KindOfSport.Handball;
                    case 0x32:
                        return KindOfSport.Basketball;
                    case 0x30:
                    case 0x33:
                    case 0x34:
                    case 0x35:
                    case 0x36:
                    case 0x37:
                    case 0x38:
                    case 0x39:
                    default:
                        return KindOfSport.Benutzerdefiniert;
                }
        }

        public override byte[] GetPayLoad()
        {
            //throw new NotImplementedException();
            return null;
        }
    }
}
