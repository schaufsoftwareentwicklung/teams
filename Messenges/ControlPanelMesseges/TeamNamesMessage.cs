﻿using S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelMessagesAbstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges.ControlPanelMesseges
{
    public abstract class TeamNamesMessage : ControlPanelMessage
    {
        public string HeimTeamDisplayName { get; set; }
        public string GastTeamDisplayName { get; set; }

        public TeamNamesMessage(byte[] payLoad) : base(payLoad)
        {
            HeimTeamDisplayName = ConvertByteToString(GetSubArray(payLoad, 0, 8));
            GastTeamDisplayName = ConvertByteToString(GetSubArray(payLoad, 7, 8));
        }

        public override byte[] GetPayLoad()
        {
            //throw new NotImplementedException();
            return null;

        }
    }
}
