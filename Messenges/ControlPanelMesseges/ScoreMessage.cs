﻿using S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelMessagesAbstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges.ControlPanelMesseges
{
    public abstract class ScoreMessage : ControlPanelMessage
    {
        public int ScoreHeim { get; set; }
        public int ScoreGast { get; set; }

        public ScoreMessage(byte[] payLoad) : base(payLoad)
        {
            ScoreHeim = ConvertByteToInt(GetSubArray(payLoad, 0, 3));
            ScoreGast = ConvertByteToInt(GetSubArray(payLoad, 2, 3));
        }

        public override byte[] GetPayLoad()
        {
            //throw new NotImplementedException();
            return null;
        }
    }
}
