﻿using S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelMessagesAbstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges.ControlPanelMesseges
{
    public abstract class GameTimeAndHornMessage : ControlPanelMessage
    {
        public DateTime GameTime { get; set; }

        public bool ShowMilliSeconds { get; set; }

        public GameTimeAndHornMessage(byte[] payLoad) : base(payLoad)
        {
            GameTime = GetGameTime(payLoad);
            AdressByte = AdressByte.GameTimeAndHorn;
        }

        private DateTime GetGameTime(byte[] payLoad)
        {
            DateTime dateTime;
            if (payLoad[4] == 0x20)
            {
                ShowMilliSeconds = false;
                dateTime = new DateTime(0000, 00, 00, 00, ConvertByteToInt(GetSubArray(payLoad, 0, 2)), ConvertByteToInt(GetSubArray(payLoad, 2, 2)));
            }
            else
            {
                ShowMilliSeconds = true;
                dateTime = new DateTime(0000, 00, 00, 00, ConvertByteToInt(GetSubArray(payLoad, 0, 2)), ConvertByteToInt(GetSubArray(payLoad, 2, 2)), ConvertByteToInt(GetSubArray(payLoad, 4, 1)) * 100);
            }

            return dateTime;
        }

        public override byte[] GetPayLoad()
        {
            //throw new NotImplementedException();
            return null;
        }
    }
}
