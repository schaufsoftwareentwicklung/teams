﻿using S_eMotion_TEAMS.ControlPanel.STX_ETX_Protokoll;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelMessagesAbstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges.ControlPanelMesseges
{
    public abstract class TeamFoulsMessage : ControlPanelMessage
    {
        public int HeimFoul { get; set; }
        public int GastFoul { get; set; }

        public TeamFoulsMessage(byte[] payLoad) : base(payLoad)
        {
            HeimFoul = ConvertByteToInt(GetSubArray(payLoad, 0, 1));
            GastFoul = ConvertByteToInt(GetSubArray(payLoad, 1, 1));
        }

        public override byte[] GetPayLoad()
        {
            //throw new NotImplementedException();
            return null;
        }
    }
}
