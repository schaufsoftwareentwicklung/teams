﻿using S_eMotion_TEAMS.Model.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Messenges
{
    public class LoadGameMessage
    {
        public Team Heim { get; set; }
        public Team Gast { get; set; }


        public LoadGameMessage(ObservableCollection<Team> teams)
        {
            Heim = teams[0];
            Gast = teams[1];
        }
    }
}
