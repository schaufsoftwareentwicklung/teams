﻿namespace S_eMotion_TEAMS.Model.Enums
{
    public enum UserTypIsNotValidTyp
    {
        LicenceIsNotValid,
        LicenceIsNoUserLicence,
        LicenceIsNotValidForVersion,
        LicenceExpired,
        SystemDoesNotMatch,
        LicenceIsValid
    }
}