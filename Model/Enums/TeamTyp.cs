﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Model.Enums
{
    public enum TeamTyp
    {
        NoInfo = 0x00,
        Heim = 0x60,
        Gast = 0x61
    }
}
