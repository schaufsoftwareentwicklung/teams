﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Model.Enums
{
    public enum GameTimeState
    {
        Running,
        Stop,
        Break
    }
}
