﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Model.Enums
{
    public enum ConnectionState
    {
        None,
        // After 1.5 seconds after not recefing a message the connection is not working
        Working,
        Error
    }
}
