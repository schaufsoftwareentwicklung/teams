﻿using S_eMotion_TEAMS.Model.Entities;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace S_eMotion_TEAMS.Entities
{
    public class Handball : Sport
    {
        public Handball()
        {
            Name = Enum.GetName(typeof(KindOfSport), KindOfSport.Handball);
            TeamSizeAsString = "12";
            MaxAktivePlayerAsString ="5";
            KindOfSport = KindOfSport.Handball;
            HeimColor = "#7167AC";
            GastColor = "#5F5389";
            IsCustomizable = false;
        }
    }
}
