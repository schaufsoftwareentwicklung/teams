﻿using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace S_eMotion_TEAMS.Model.Entities
{
    public class CustomSport : Sport
    {
        public CustomSport()
        {
            Name = Enum.GetName(typeof(KindOfSport), KindOfSport.Benutzerdefiniert);
            TeamSizeAsString = Properties.Settings.Default.CustomSportTeamSize;
            MaxAktivePlayerAsString = Properties.Settings.Default.CustomSportAktivePlayer;
            KindOfSport = KindOfSport.Benutzerdefiniert;
            //HeimColor = Properties.Settings.Default.CustomSportHeimColor;
            //GastColor = Properties.Settings.Default.CustomSportHeimColor;
            HeimColor = "#4ABED5";
            GastColor = "#3D9DAD";
            IsCustomizable = true;
        }
    }
}
