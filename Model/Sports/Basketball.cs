﻿using S_eMotion_TEAMS.Model.Entities;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace S_eMotion_TEAMS.Entities
{
    public class Basketball : Sport
    {
        public Basketball()
        {
            Name = Enum.GetName(typeof(KindOfSport), KindOfSport.Basketball);
            TeamSizeAsString = "10";
            MaxAktivePlayerAsString = "5";
            KindOfSport = KindOfSport.Basketball;
            HeimColor = "#EA503A";
            GastColor = "#B53B29";
            IsCustomizable = false;
        }
    }
}
