﻿using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelReceivedMesseges;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Model.Entities
{
    public class Team : INotifyPropertyChanged, ICloneable
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<Player> _squad;
        private ObservableCollection<Player> _teamMember;
        private string _displayName;
        private string _teamName;

        public string KindOfSportAsString
        {
            get => Enum.GetName(typeof(KindOfSport), KindOfSport);
            set
            {
                KindOfSport = (KindOfSport)Enum.Parse(typeof(KindOfSport), value);
            }
        }

        [JsonIgnore]
        public KindOfSport KindOfSport { get; set; }

        public string TeamName
        {
            get => _teamName;
            set
            {
                _teamName = value;
                OnPropertyChanged(nameof(TeamName));
            }
        }
        public string DisplayName
        {
            get => _displayName;
            set
            {
                _displayName = value;
                OnPropertyChanged(nameof(DisplayName));
            }
        }

        private Player _latestFouler;
        private Timer _latestFoulerTimer;

        public ObservableCollection<Player> TeamMember
        {
            get => _teamMember;
            set
            {
                _teamMember = value;
                OnPropertyChanged(nameof(TeamMember));

                if (_teamMember != null)
                {
                    foreach (Player player in _teamMember)
                        player.FoulRaised += PlayerFoulRaised;
                }
            }
        }

        public Team()
        {
            _latestFoulerTimer = new Timer(ResetLatestFouler, null, Timeout.Infinite, Timeout.Infinite);
        }

        private void PlayerFoulRaised(object sender, Player e)
        {
            _latestFoulerTimer.Change(Timeout.Infinite, Timeout.Infinite); ;
            if (_latestFouler != null)
            {
                _latestFouler.IsLatestFouler = false;
                _latestFouler = e;
            }
            else
                _latestFouler = e;

            _latestFoulerTimer.Change(Properties.Settings.Default.DurationLatestFouler * 1000, Timeout.Infinite);
        }

        private void ResetLatestFouler(object state)
        {
            _latestFouler.IsLatestFouler = false;
            _latestFouler = null;
        }

        public ObservableCollection<Player> Squad
        {
            get => _squad;
            set
            {
                _squad = value;
                OnPropertyChanged(nameof(Squad));
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public void IsAktivePlayerChanged(int maxAktivePlayer)
        {
            if (TeamMember.Where(x => x.IsAktive == true).ToList().Count() < maxAktivePlayer)
                TeamMember.Where(x => x.IsAktive == false).ToList().ForEach(x => x.IsAktiveEnabled = false);
            else
                TeamMember.Where(x => x.IsAktive == false).ToList().ForEach(x => x.IsAktiveEnabled = true);
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public void Reset()
        {
            foreach (Player player in TeamMember)
            {
                player.Foul = 0;
                player.Score = 0;
            }
        }

        internal void SetPlayerPositionIndexes()
        {
            for (int i = 0; i < TeamMember.Count; i++)
                TeamMember[i].PositionIndex = i;
        }
    }
}
