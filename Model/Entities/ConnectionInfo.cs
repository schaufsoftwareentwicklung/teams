﻿using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Model.Entities
{
    public class ConnectionInfo : INotifyPropertyChanged
    {
        private ConnectionState _connectionState;

        public event PropertyChangedEventHandler PropertyChanged;

        public ConnectionState ConnectionState
        {
            get => _connectionState;
            set
            {
                _connectionState = value;
                OnPropertyChanged(nameof(IconPath));
                OnPropertyChanged(nameof(Description));
            }
        }
        public string PortName { get => Properties.Settings.Default.SelectedComPort; }
        public Timer Timer { get; set; }

        public string IconPath
        {
            get
            {
                switch (ConnectionState)
                {
                    case Enums.ConnectionState.None:
                        return "/Ressources/Icons/Connection/Connection_NoInfo.png";
                    case Enums.ConnectionState.Working:
                        return "/Ressources/Icons/Connection/Connection_Working.png";
                    case Enums.ConnectionState.Error:
                        return "/Ressources/Icons/Connection/Connection_Error.png";
                    default:
                        return "/Ressources/Icons/Connection/Connection_NoInfo.png";
                }
            }
        }

        public string Description
        {
            get
            {
                switch (ConnectionState)
                {
                    case Enums.ConnectionState.None:
                        return "Es wurde keine Verbindung aufgebaut oder Gefunden";
                    case Enums.ConnectionState.Working:
                        return "Die Verbindung zu Bedienpult ist beständig.";
                    case Enums.ConnectionState.Error:
                        return "Es gibt Verbindungsprobleme zum Bedienpult";
                    default:
                        return "Es wurde keine Verbindung aufgebaut oder Gefunden";
                }
            }
        }

        public ConnectionInfo()
        {
        }

        public void InitializeTimer()
        {
            Timer = new Timer(Callback, null, 1000, Timeout.Infinite);
        }

        private void Callback(Object state)
        {
            ConnectionState = Enums.ConnectionState.Working;

            Timer.Change(1500, Timeout.Infinite);
        }

        public void TimerReset()
        {
            ConnectionState = Enums.ConnectionState.Working;
            Timer.Change(1500, Timeout.Infinite);
        }

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
