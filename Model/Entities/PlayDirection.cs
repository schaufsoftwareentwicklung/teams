﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Model.Entities
{
    public enum PlayDirection
    {
        None = 0x30,
        Left = 0x31,
        Right = 0x32
    }
}
