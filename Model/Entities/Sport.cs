﻿using GalaSoft.MvvmLight.Messaging;
using S_eMotion_TEAMS.Messenges;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows.Media;

namespace S_eMotion_TEAMS.Model.Entities
{
    public abstract class Sport
    {
        private string _TeamSize;
        private string _MaxAktivePlayer;

        public string Name { get; set; }
        public string TeamSizeAsString
        {
            get => _TeamSize;
            set
            {
                value = value.Trim();
                if (string.IsNullOrWhiteSpace(value) || !int.TryParse(value, out int outVal))
                    value = "0";
                _TeamSize = value;
                Properties.Settings.Default.CustomSportTeamSize = value;
                Properties.Settings.Default.Save();
                Messenger.Default.Send(new MaxSportTeamSizeChangedMessage());
            }

        }
        public string MaxAktivePlayerAsString
        {
            get => _MaxAktivePlayer;
            set
            {
                value = value.Trim();
                if (string.IsNullOrWhiteSpace(value) || !int.TryParse(value, out int outVal))
                    value = "0";
                _MaxAktivePlayer = value;
                Properties.Settings.Default.CustomSportAktivePlayer = value;
                Properties.Settings.Default.Save();
                Messenger.Default.Send(new MaxSportAktivePlayerChangedMessage());
            }

        }

        public bool IsCustomizable { get; set; }

        public KindOfSport KindOfSport { get; set; }
        public string HeimColor { get; set; }
        public string GastColor { get; set; }

        public int TeamSize { get => int.Parse(TeamSizeAsString); }
        public int MaxAktivePlayer { get => int.Parse(MaxAktivePlayerAsString); }


    }
}
