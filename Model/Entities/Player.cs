﻿using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using S_eMotion_TEAMS.Messenges;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelReceivedMesseges;
using S_eMotion_TEAMS.Messenges.ControlPanelMesseges.ControlPanelSendMessages;
using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace S_eMotion_TEAMS.Model.Entities
{
    public class Player : INotifyPropertyChanged
    {
        [JsonIgnore]
        public RelayCommand SubtractFoul_1_Command { get; private set; }
        [JsonIgnore]
        public RelayCommand RaiseFoul_1_Command { get; private set; }
        [JsonIgnore]
        public RelayCommand SubtractScore_1_Command { get; private set; }
        [JsonIgnore]
        public RelayCommand RaiseScore_1_Command { get; private set; }
        [JsonIgnore]
        public RelayCommand RaiseScore_2_Command { get; private set; }
        [JsonIgnore]
        public RelayCommand RaiseScore_3_Command { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<Player> FoulRaised;

        public bool IsLatestFouler = false;
        public bool IsUpdating = false;
        public bool IsLoading => FoulsAsString != null && NumberAsString != null && Name != null && ScoreAsString != null;

        private bool _isAktive;
        private bool _isAktiveEnabled;
        private string _numberAsString;
        private string _name;
        private string _scoreAsString;
        private string _foulsAsString;



        public bool IsAktive
        {
            get => _isAktive;
            set
            {
                _isAktive = value;
                Messenger.Default.Send(new IsAktivePlayerChangedMessage());
                Messenger.Default.Send(new GameRelevantInfoChangedMessage());
                OnPropertyChanged(nameof(IsAktive));

                if (!IsUpdating && IsLoading)
                    Messenger.Default.Send(new PlayerSendMessage(this));
            }
        }

        [JsonIgnore]
        public bool IsAktiveEnabled
        {
            //inverted because of Property "IsEnabled"
            get => !_isAktiveEnabled;
            set
            {
                _isAktiveEnabled = value;
                OnPropertyChanged(nameof(IsAktiveEnabled));
            }
        }
        [JsonPropertyName("Number")]
        public string NumberAsString
        {
            get => string.IsNullOrWhiteSpace(_numberAsString) ? "0" : _numberAsString;

            set
            {
                value = value.Trim();
                if (string.IsNullOrWhiteSpace(value) || !int.TryParse(value, out _))
                    value = "0";

                Messenger.Default.Send(new GameRelevantInfoChangedMessage());
                _numberAsString = value;
            }
        }
        public string Name
        {
            get => _name;
            set
            {
                value = value.Trim();
                _name = value;
                Messenger.Default.Send(new GameRelevantInfoChangedMessage());
            }
        }
        [JsonPropertyName("Score")]
        public string ScoreAsString
        {
            get => string.IsNullOrWhiteSpace(_scoreAsString) ? "0" : _scoreAsString;
            set
            {
                _scoreAsString = value;
            }
        }
        [JsonPropertyName("Fouls")]
        public string FoulsAsString
        {
            get => string.IsNullOrWhiteSpace(_foulsAsString) ? "0" : _foulsAsString;
            set
            {
                _foulsAsString = value;
            }
        }

        [JsonIgnore]
        public int Number
        {
            get => int.Parse(_numberAsString);
            set
            {
                _numberAsString = value.ToString();
                OnPropertyChanged(nameof(NumberAsString));
            }
        }
        [JsonIgnore]
        public int Foul
        {
            get => int.Parse(_foulsAsString);
            set
            {
                _foulsAsString = value.ToString();
                OnPropertyChanged(nameof(FoulsAsString));
                SubtractFoul_1_Command.RaiseCanExecuteChanged();
                RaiseFoul_1_Command.RaiseCanExecuteChanged();
                IsLatestFouler = true;
                Messenger.Default.Send(new GameRelevantInfoChangedMessage());

                if (!IsUpdating && !IsLoading)
                    Messenger.Default.Send(new PlayerSendMessage(this));
            }
        }
        [JsonIgnore]
        public int Score
        {
            get => int.Parse(_scoreAsString);
            set
            {
                _scoreAsString = value.ToString();
                OnPropertyChanged(nameof(ScoreAsString));
                SubtractScore_1_Command.RaiseCanExecuteChanged();
                RaiseScore_1_Command.RaiseCanExecuteChanged();
                RaiseScore_2_Command.RaiseCanExecuteChanged();
                RaiseScore_3_Command.RaiseCanExecuteChanged();
                Messenger.Default.Send(new GameRelevantInfoChangedMessage());

                if (!IsUpdating && IsLoading)
                    Messenger.Default.Send(new PlayerSendMessage(this));
            }
        }

        [JsonIgnore]
        public TeamTyp TeamTyp { get; set; }
        [JsonIgnore]
        public int PositionIndex { get; set; }

        public Player()
        {
            SubtractFoul_1_Command = new RelayCommand(SubtractFoul_1, CanSubtractFoul_1);
            RaiseFoul_1_Command = new RelayCommand(RaiseFoul_1, CanRaiseFoul_1);
            SubtractScore_1_Command = new RelayCommand(SubtractScore_1, CanSubtractScore_1);
            RaiseScore_1_Command = new RelayCommand(RaiseScore_1, CanRaiseScore_1);
            RaiseScore_2_Command = new RelayCommand(RaiseScore_2, CanRaiseScore_2);
            RaiseScore_3_Command = new RelayCommand(RaiseScore_3, CanRaiseScore_3);
        }

        private bool CanRaiseScore_3()
        {
            return Score < 97;
        }
        private void RaiseScore_3()
        {
            Score += 3;
        }


        private bool CanRaiseScore_2()
        {
            return Score < 98;
        }
        private void RaiseScore_2()
        {
            Score += 2;
        }



        private bool CanRaiseScore_1()
        {
            return Score < 99;
        }
        private void RaiseScore_1()
        {
            Score += 1;
        }

        private bool CanSubtractScore_1()
        {
            return Score > 0;
        }
        private void SubtractScore_1()
        {
            Score -= 1;
        }

        private bool CanRaiseFoul_1()
        {
            return Foul < 5;
        }
        private void RaiseFoul_1()
        {
            Foul += 1;
            OnFoulRaised(this);
        }

        private bool CanSubtractFoul_1()
        {
            return Foul > 0;
        }
        private void SubtractFoul_1()
        {
            Foul -= 1;
        }

        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        protected virtual void OnFoulRaised(Player player)
        {
            FoulRaised?.Invoke(this, player);
        }

        internal void Update(PlayerReceivedMessage action)
        {
            IsUpdating = true;
            IsAktive = action.IsAktive;
            Name = action.Name;
            Number = action.Number;
            Score = action.Score;
            Foul = action.Foul;
            if (action.ValidFlag && !IsLatestFouler)
            {
                OnFoulRaised(this);
                IsLatestFouler = true;
            }
            IsUpdating = false;
        }
    }
}
