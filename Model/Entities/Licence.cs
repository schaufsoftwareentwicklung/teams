﻿using S_eMotion_TEAMS.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security;
using System.Security.Cryptography;

namespace S_eMotion_TEAMS.Model.Entities
{
    public class Licence
    {
        private const string ENCRYPTION_KEY = "HFTSLZ3VARQXUDY79N15EBW6C4M2KPG8";
        private const string BITLOC = "MXEIAIQFFRDXXRKRRTXRTRFQFEEHRXXQQRKTFTGXRASFXQPJRGEDFXSNRXIOXEIFFBQQKXRGXRLSRLXONEHEACFXRRXMMTTRFQEXBRXQETQULSSLUXEFSQNXJOXRSFXFUPHEJQXSLESGRXMKEQXBEEXFUQQFCUXNXEHEXQPFESXUSLQOFNXSTQGTXQXMEUUXQAJXFXPTXTKTEKHBFX";
        private const string CHARS_AND_DIGITS = "ABDEFGHIJLMNOPQRSTUWYZ0123456789";


        public bool IsValid = false;
        public string Key;


        public string Type { get; set; }
        public string Permission { get; set; }
        public string Plugins { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string MajorVersion { get; set; }
        public string MinorVersion { get; set; }
        public string Customer { get; set; }
        public string System { get; set; }
        public string CheckMin { get; set; }
        public string CheckMax { get; set; }

        public string MasterSalt { get; set; }
        public LicenceType LicenceType
        {
            get
            {
                if (System == "0")
                    return LicenceType.Master;
                else
                    return LicenceType.User;
            }
        }




        public Licence(string key)
        {
            Key = key;
            MasterSalt = string.Empty;
            IsValid = Decode(key);

            if (IsValid)
            {
                GetCheckMin();
                GetCheckMax();
            }
        }


        public string GetAuthenticationCode()
        {
            if (!IsValid || System != "0")
                return null;

            string auth = string.Empty;
            int salt = GetSystemSalt();

            auth += RandomStringFromSystemSalt(salt, CHARS_AND_DIGITS);
            auth += Customer;
            auth += "K";
            auth += salt;
            auth += "V";
            auth += RandomStringFromSystemSalt(salt, CHARS_AND_DIGITS);
            auth += "C";
            auth += Permission;
            auth += CheckMax;
            auth += "X";
            auth += CheckMin;

            return auth;
        }

        public UserTypIsNotValidTyp ValidateUserKey()
        {
            if (!IsValid)
                return UserTypIsNotValidTyp.LicenceIsNotValid;
            if (System == "0")
                return UserTypIsNotValidTyp.LicenceIsNoUserLicence;
            if (System != GetSystemSalt().ToString())
                return UserTypIsNotValidTyp.SystemDoesNotMatch;
            if (GetEndDate() < DateTime.Now)
                return UserTypIsNotValidTyp.LicenceExpired;
            if (Convert.ToDouble(Properties.Resources.Version) > GetEndVersion())
                return UserTypIsNotValidTyp.LicenceIsNotValidForVersion;

            return UserTypIsNotValidTyp.LicenceIsValid;
        }

        public DateTime GetEndDate()
        {
            return new DateTime(Convert.ToInt32(Year), Convert.ToInt32(Month), Convert.ToInt32(Day));
        }

        public double GetEndVersion()
        {
            return Convert.ToDouble(string.Format("{0}.{1}", MajorVersion, MinorVersion));
        }



        private bool Decode(string masterKey)
        {
            masterKey = masterKey.ToUpper();
            masterKey = masterKey.Replace("-", "");


            for (int i = 0; i < masterKey.Length; i++)
            {
                int indexInEncryption_KEY = ENCRYPTION_KEY.IndexOf(masterKey[i]);
                string sum = "00000" + Convert.ToString(indexInEncryption_KEY, 2);
                MasterSalt += sum.Substring(sum.Length - 5);
            }

            string type = ""; string type2 = "";
            string permission = ""; string permission2 = "";
            string plugins = ""; string plugins2 = "";

            string day = ""; string day2 = "";
            string month = ""; string month2 = "";
            string year = ""; string year2 = "";

            string major = ""; string major2 = "";
            string minor = ""; string minor2 = "";

            string customer = ""; string customer2 = "";
            string system = ""; string system2 = "";

            for (int i = 0; i < MasterSalt.Length; i++)
            {

                if (BITLOC[i] == 'A') { type += MasterSalt[i]; }
                if (BITLOC[i] == 'B') { type2 += MasterSalt[i]; }

                if (BITLOC[i] == 'C') { permission += MasterSalt[i]; }
                if (BITLOC[i] == 'D') { permission2 += MasterSalt[i]; }

                if (BITLOC[i] == 'E') { plugins += MasterSalt[i]; }
                if (BITLOC[i] == 'F') { plugins2 += MasterSalt[i]; }

                if (BITLOC[i] == 'G') { day += MasterSalt[i]; }
                if (BITLOC[i] == 'H') { day2 += MasterSalt[i]; }

                if (BITLOC[i] == 'I') { month += MasterSalt[i]; }
                if (BITLOC[i] == 'J') { month2 += MasterSalt[i]; }

                if (BITLOC[i] == 'K') { year += MasterSalt[i]; }
                if (BITLOC[i] == 'L') { year2 += MasterSalt[i]; }

                if (BITLOC[i] == 'M') { major += MasterSalt[i]; }
                if (BITLOC[i] == 'N') { major2 += MasterSalt[i]; }

                if (BITLOC[i] == 'O') { minor += MasterSalt[i]; }
                if (BITLOC[i] == 'P') { minor2 += MasterSalt[i]; }

                if (BITLOC[i] == 'Q') { customer += MasterSalt[i]; }
                if (BITLOC[i] == 'R') { customer2 += MasterSalt[i]; }

                if (BITLOC[i] == 'S') { system += MasterSalt[i]; }
                if (BITLOC[i] == 'T') { system2 += MasterSalt[i]; }
            }

            if (type == ReverseString(type2) &&
                permission == ReverseString(permission2) &&
                plugins == ReverseString(plugins2) &&
                day == ReverseString(day2) &&
                month == ReverseString(month2) &&
                year == ReverseString(year2) &&
                major == ReverseString(major2) &&
                minor == ReverseString(minor2) &&
                customer == ReverseString(customer2) &&
                system == ReverseString(system2))
            {
                Type = Convert.ToInt32(type, 2).ToString();
                Permission = Convert.ToInt32(permission, 2).ToString();
                Plugins = ReverseString(plugins);
                Day = Convert.ToInt32(day, 2).ToString();
                Month = Convert.ToInt32(month, 2).ToString();
                Year = (Convert.ToInt32(year, 2) + 2019).ToString();
                MajorVersion = Convert.ToInt32(major, 2).ToString();
                MinorVersion = Convert.ToInt32(minor, 2).ToString();
                Customer = Convert.ToInt32(customer, 2).ToString();
                System = Convert.ToInt32(system, 2).ToString();

                return true;
            }
            else
                return false;
        }

        private string ReverseString(string s)
        {
            char[] array = s.ToCharArray();
            Array.Reverse(array);
            return new string(array);
        }

        private int GetSystemSalt()
        {
            ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection moc = mc.GetInstances();
            string MACAddress = string.Empty;
            foreach (ManagementObject mo in moc)
            {
                if (MACAddress == string.Empty)
                {
                    if ((bool)mo["IPEnabled"] == true) MACAddress = mo["MacAddress"].ToString();
                }
                mo.Dispose();
            }

            MACAddress = MACAddress.Replace(":", "");


            string systemsalt = string.Join(string.Empty, MACAddress.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));

            systemsalt = "00000000000" + systemsalt;
            systemsalt = systemsalt.Substring(systemsalt.Length - 11);
            if (systemsalt == "00000000000")
                systemsalt = "00000000001";
            return Convert.ToInt32(systemsalt, 2);
        }

        private static string RandomStringFromSystemSalt(int salt, string type)
        {
            string rngString = string.Empty;

            for (int i = 0; i < salt.ToString().Length; i++)
            {
                int index = Convert.ToInt32(salt.ToString()[i]);
                while (index >= type.Length)
                {
                    index -= type.Length;
                }

                rngString += type[index];
            }

            return rngString;
        }

        private string GetPropertyAsBinaryString(string convertBase, string property, int stringLength)
        {
            convertBase += Convert.ToString(Convert.ToInt32(property), 2);
            return convertBase.Substring(convertBase.Length - stringLength);
        }



        private void GetCheckMin()
        {
            int checksum = 0;
            foreach (string property in GetPropertyAsBinaryString())
            {
                for (int i = 0; i < property.Length; i++)
                {
                    if (property[i] == '1')
                        checksum++;
                }
            }

            CheckMin = checksum.ToString();
        }

        private List<string> GetPropertyAsBinaryString()
        {
            var list = new List<string>()
                    {
                           GetPropertyAsBinaryString("0000",Type,4),
                           GetPropertyAsBinaryString("00",Permission,2),
                           ReverseString(Plugins),
                           GetPropertyAsBinaryString("00000",Day,5),
                           GetPropertyAsBinaryString("0000",Month,4),
                           GetPropertyAsBinaryString("0000",(Convert.ToInt32(Year) -2019).ToString(),4),
                           GetPropertyAsBinaryString("00000",MajorVersion,5),
                           GetPropertyAsBinaryString("0000",MinorVersion,4),
                           GetPropertyAsBinaryString("00000000000000000000",Customer,20),
                           GetPropertyAsBinaryString("000000000000",System,12)
                    };

            for (int i = list.Count - 1; i >= 0; i--)
            {
                list.Insert(i + 1, ReverseString(list[i]));
            }

            return list;
        }

        private void GetCheckMax()
        {
            int checkMax = 0;

            foreach (char c in MasterSalt)
            {
                if (c == '1')
                    checkMax++;
            }

            CheckMax = checkMax.ToString();
        }



        public override string ToString()
        {
            string licence = string.Empty;

            licence += string.Format("{0} = {1}\n", nameof(Type), string.IsNullOrWhiteSpace(Type) ? "N/A" : Type);
            licence += string.Format("{0} = {1}\n", nameof(Permission), string.IsNullOrWhiteSpace(Permission) ? "N/A" : Permission);
            licence += string.Format("{0} = {1}\n", nameof(Plugins), string.IsNullOrWhiteSpace(Plugins) ? "N/A" : Plugins);
            licence += string.Format("{0} = {1}\n", nameof(Day), string.IsNullOrWhiteSpace(Day) ? "N/A" : Day);
            licence += string.Format("{0} = {1}\n", nameof(Month), string.IsNullOrWhiteSpace(Month) ? "N/A" : Month);
            licence += string.Format("{0} = {1}\n", nameof(Year), string.IsNullOrWhiteSpace(Year) ? "N/A" : Year);
            licence += string.Format("{0} = {1}\n", nameof(MajorVersion), string.IsNullOrWhiteSpace(MajorVersion) ? "N/A" : MajorVersion);
            licence += string.Format("{0} = {1}\n", nameof(MinorVersion), string.IsNullOrWhiteSpace(MinorVersion) ? "N/A" : MinorVersion);
            licence += string.Format("{0} = {1}\n", nameof(Customer), string.IsNullOrWhiteSpace(Customer) ? "N/A" : Customer);
            licence += string.Format("{0} = {1}\n", nameof(System), string.IsNullOrWhiteSpace(System) ? "N/A" : System);

            return licence;
        }
    }
}